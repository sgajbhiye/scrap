<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Scrap Delivery</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <!-- <?= $this->Html->css('bootstrap.min.css')?> -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!--  Material Dashboard CSS    -->
    <?= $this->Html->css('material-dashboard.css')?>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <?= $this->Html->css('demo.css')?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!--  CSS for Demo Purpose, don't include it in your project -->
    <?= $this->Html->css('style.css') ?>
    <!--     Fonts and icons     -->
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
    <?php if($this->request->param('action') == 'alluser' || $this->request->param('action') == 'driverlist' || $this->request->param('action') == 'complete' || $this->request->param('action') =='allcomplete' || $this->request->param('action') == ('alluploads') || $this->request->param('action') == ('allpending') || $this->request->param('action') == ('allassigned') || $this->request->param('action') == ('allinprocess') || $this->request->param('action') == ('tasks') || $this->request->param('action') == ('pending') || $this->request->param('action') == ('assigned') || $this->request->param('action') == ('inprocess')){ ?>
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
    <?php } ?>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>
    <body>
        <div class="wrapper">
            <?= $this->element('dashsidebar') ?>
            <div class="main-panel">
                <?= $this->element('dashheader') ?>
                <?= $this->fetch('content') ?>
                <?= $this->element('footer') ?>
            </div>
        </div>
        <?= $this->Html->script('jquery.min.js') ?>
        <?= $this->Html->script('bootstrap.min.js') ?>
        <?= $this->Html->script('material.min.js') ?>
        <!-- <?= $this->Html->script('chartist.min.js') ?> -->
        <?= $this->Html->script('bootstrap-notify.js') ?>
        <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADs6GPp3v0dzH6ZJ-JLHgHU83B5DqXLXU"></script> -->
        <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <?= $this->Html->script('material-dashboard.js') ?>
        <!-- <?= $this->Html->script('demo.js') ?> -->
        <?php if($loggeduser){ ?>
            <?= $this->Html->script('userdashboard.js') ?>
        <?php } ?>
        <?php if($admin || $loggedagency){ ?>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
            <?= $this->Html->script('citychange.js') ?>
        <?php } ?>
        <?php if($this->request->param('action') == 'alluser' || $this->request->param('action') == 'driverlist' || $this->request->param('action') == 'complete' || $this->request->param('action') == 'allcomplete' || $this->request->param('action') == ('alluploads') || $this->request->param('action') == ('allpending') || $this->request->param('action') == ('allassigned') || $this->request->param('action') == ('allinprocess') || $this->request->param('action') == ('tasks') || $this->request->param('action') == ('pending') || $this->request->param('action') == ('assigned') || $this->request->param('action') == ('inprocess')){ ?>
            <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
            <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js" type="text/javascript"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
            <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js" type="text/javascript"></script>
            <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js" type="text/javascript"></script>
            <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
            <!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script> -->
        <?php } ?>
        <?php if($this->request->param('action') == 'tasks' || $this->request->param('action') == 'pending' || $this->request->param('action') == 'assigned' || $this->request->param('action') == 'inprocess' || $this->request->param('action') == 'complete'){ ?>
            <?= $this->Html->script('newuploadtask.js') ?>
        <?php } ?>
        <?php if($this->request->param('action') == 'alluploads' || $this->request->param('action') == 'allpending' || $this->request->param('action') == 'allassigned' || $this->request->param('action') == 'allinprocess' || $this->request->param('action') == 'allcomplete'){ ?>
            <?= $this->Html->script('uploadsforadmin.js') ?>            
        <?php } ?>
        <?php if($this->request->param('action') == 'viewmap'){ ?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBA2NxpTKWg2OJ5isRv9R6AUmWZ3zEkNSQ&callback=initMap"
                async defer></script>
        <?php } ?>
        <script>
            $( function() {
                var d = new Date();
                var year = d.getFullYear() - 18;
                $( "#datepicker" ).datepicker({ 
                    changeYear: true, 
                    changeMonth: true,
                    dateFormat: 'dd-mm-yy', 
                    yearRange: '1947:' + year + '', 
                });
                $("#startDateId, #endDateId").datepicker({ 
                    changeYear: true, 
                    changeMonth: true,
                    dateFormat: 'dd-mm-yy', 
                });
            });
        </script>
        <!-- Modal -->
        <div class="modal fade" id="scrapImgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <!--Content-->
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title w-100" id="myModalLabel">Scrap Details</h4>
                    </div>
                    <!--Body-->
                    <div class="modal-body text-center">
                        <div class="row">
                            <div class="col-md-6">
                                <img id="scrapImgId" src="<?= $this->request->webroot ?>img/demoscrap.png" alt="Uploaded_Material" class="img-responsive">                                
                            </div>
                            <div class="col-md-6">
                                <dl class="row">
                                    <dt class="col-md-4 text-right">Description :</dt>
                                    <dd class="col-md-8 text-left" id="descripId"></dd>

                                    <dt class="col-md-4 text-right">Uploaded By :</dt>
                                    <dd class="col-md-8 text-left" id="scrapUploadUserId"></dd>

                                    <dt class="col-md-4 text-right">Mobile :</dt>
                                    <dd class="col-md-8 text-left" id="scrapUserMobId"></dd>

                                    <dt class="col-md-4 text-right">Category :</dt>
                                    <dd class="col-md-8 text-left" id="categoryId"></dd>

                                    <dt class="col-md-4 text-right">Address :</dt>
                                    <dd class="col-md-8 text-left" id="addressId"></dd>

                                    <dt class="col-md-4 text-right">City :</dt>
                                    <dd class="col-md-8 text-left" id="cityId"></dd>
                                </dl> 
                            </div>
                        </div>
                    </div>
                    <!--Footer-->
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                </div>
                <!--/.Content-->
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg" id="scrapAssignModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <!--Content-->
                <div class="modal-content">
                    <!--Header-->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title w-100" id="myModalLabel">Assign Scrap To Driver</h4>
                    </div>
                    <!--Body-->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img id="ascrapImgId" src="<?= $this->request->webroot ?>img/demoscrap.png" alt="Uploaded_Material" class="img-responsive">
                            </div>
                            <div class="col-md-6">                                
                                <dl class="row">
                                    <dt class="col-md-4 text-right">Description :</dt>
                                    <dd class="col-md-8 text-left" id="adescripId">hello</dd>

                                    <dt class="col-md-4 text-right">Uploaded By :</dt>
                                    <dd class="col-md-8 text-left" id="ascrapUploadUserId">hello</dd>

                                    <dt class="col-md-4 text-right">Mobile :</dt>
                                    <dd class="col-md-8 text-left" id="ascrapUserMobId">hello</dd>

                                    <dt class="col-md-4 text-right">Category :</dt>
                                    <dd class="col-md-8 text-left" id="acategoryId">hello</dd>

                                    <dt class="col-md-4 text-right">Address :</dt>
                                    <dd class="col-md-8 text-left" id="aaddressId">hello</dd>

                                    <dt class="col-md-4 text-right">City :</dt>
                                    <dd class="col-md-8 text-left" id="acityId">hello</dd>                               
                                </dl>
                                <hr>
                                <dl>
                                    <dt class="col-md-4 text-right">Select Driver :</dt>
                                    <dd class="col-md-8 text-left">
                                        <select class="selectinpt" id="assignedDriverId" style="margin-top: 0;">
                                            <option></option>
                                        <?php foreach ($alldrivers as $keys => $values) { ?>
                                            <option value="<?= $values['id'] ?>"><?= $values['user']['fname'] ." ". $values['user']['lname'] ?></option>
                                        <?php } ?>
                                        </select>                                        
                                    </dd>
                                    <input type="hidden" id="ascrapId" value="">
                                    <dt class="col-md-4 text-right"><button id="assignDriverPopBtnId" type="button" class="btn btn-primary" title="Click to Assign" style="will-change: initial;">Assign</button></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <!--Footer-->
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                </div>
                <!--/.Content-->
            </div>
        </div>       
    </body>
</html>
