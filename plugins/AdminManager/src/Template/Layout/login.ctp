<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Scrap Delivery</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <?= $this->Html->css('bootstrap.min.css')?>
    <!--  Material Dashboard CSS    -->
    <?= $this->Html->css('material-dashboard.css')?>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <?= $this->Html->css('demo.css')?>

    <!--  CSS for Demo Purpose, don't include it in your project -->
    <?= $this->Html->css('style.css') ?>
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>
    <body>
        <?= $this->fetch('content') ?>
        
        <?= $this->Html->script('jquery.min.js') ?>
        <?= $this->Html->script('bootstrap.min.js') ?>
        <?= $this->Html->script('material.min.js') ?>
        <?= $this->Html->script('bootstrap-notify.js') ?>
        <!-- <?= $this->Html->script('material-dashboard.js') ?> -->
    </body>
</html>
