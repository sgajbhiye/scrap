function openDetailsModalForAdmin(desc,nameofuser,mobile,category,address,city,imgpath){
    $("#scrapImgModal").modal('show');
    $("#descripId").text(desc);
    $("#scrapUploadUserId").text(nameofuser);
    $("#scrapUserMobId").text(mobile);
    $("#categoryId").text(category);
    $("#addressId").text(address);
    $("#cityId").text(city);
    var path = imgpath.trim();
    if(imgpath != null || imgpath != '' || imgpath != 'undefined'){
        $("#scrapImgId").attr('src',configpath['globalPath'] + path);
    }
}