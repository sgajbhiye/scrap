$(document).ready(function() {
    $('#agencylist').DataTable({
        "paging": false,
        // "ordering": false,
        "info": false,
        columnDefs: [{
            targets: -1,
            visible: true,
            searchable: false,
            orderable: false
        }]
    });
    $('#exampleuser, #driverlist, #completeTasksListId, #assignedTasksListId, #inProcessTasksListId').DataTable({
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength','colvis'           	
        // ],
        buttons: [
            'pageLength',
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [0, ':visible']
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible',
                    columns: ':not(:last-child)'
                }
            }
            // ,
            // {
            //     extend: 'pdfHtml5',
            //     exportOptions: {
            //         columns: ':visible',
            //         columns: ':not(:last-child)'
            //     }
            // }
            // ,'colvis'
        ],
        columnDefs: [{
            targets: -1,
            visible: true,
            searchable: false,
            orderable: false
        }]
    });
    $("#newTasksListId").DataTable({
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        buttons: [
            'pageLength',
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [0, ':visible']
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible',
                    columns: [0,2,3,4,5,6,7,8]
                }
            }
        ],
        columnDefs: [{
            targets: -1,
            visible: true,
            searchable: false,
            orderable: false
        }]
    });
    $("#pendingTasksListId").DataTable({
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        buttons: [
            'pageLength',
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [0, ':visible']
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible',
                }
            }
        ],
        columnDefs: [{
            targets: -1,
            visible: true,
            searchable: false,
            orderable: false
        }]
    });
    $('#categorylist').DataTable({
        columnDefs: [{
            targets: -1,
            visible: true,
            searchable: false,
            orderable: false
        }]
    });
    $('#agencyDriversListId').DataTable({
        // "paging":   false,
        // "ordering": false,
        // "info":     false,
        columnDefs: [{
            targets: -1,
            visible: true,
            searchable: false,
            orderable: false
        }]
    });
    $("#cityId").change(function() {
        var state = $("#cityId option:selected").attr('atr_state');
        $("#stateId").val(state);
        $("#stateId").focus();
        if ($("#stateId").val() != '') {
            $("#floatLabDivId").removeClass('is-empty');
        } else {
            $("#floatLabDivId").addClass('is-empty');
        }
    });
    $("#limitId").change(function() {
        $("#limitFormId").submit();
    });
});