function openScrapDetailModal(index){
	$.each(uploadTaskObj, function(key,val) {
		// console.log(val);
        if(key == index){
        	if(val.scrapimg != ""){
        		$("#scrapImgId").attr('src', configpath['globalPath'] + val.scrapimg);
        	}
       		$("#descripId").text(val.description);
       		$("#scrapUploadUserId").text(val.user.fname+" "+val.user.lname);
       		$("#scrapUserMobId").text(val.user.mobile);
       		$("#categoryId").text(val.category.name);
       		if(val.address == null || val.address == ""){
      		    $("#addressId").text("Not Specified");      
      		}else{
      	    	$("#addressId").text(val.address);   
      		}
      	    if(val.city_id == null || val.city_id == ""){
        	    $("#cityId").text("Not Specified");
        	}else{
        	    $("#cityId").text(val.city.name);
        	}
        }
    });
	$("#scrapImgModal").modal('show');	
}

function openScrapDetailModalForApproved(index){
	$.each(approvedTaskObj, function(key,val) {
        if(key == index){
        	if(val.scrapimg != ""){
        		$("#ascrapImgId").attr('src', configpath['globalPath'] + val.scrapimg);
        	}
        	$("#ascrapId").val(val.id);
       		$("#adescripId").text(val.description);
       		$("#ascrapUploadUserId").text(val.user.fname+" "+val.user.lname);
       		$("#ascrapUserMobId").text(val.user.mobile);
       		$("#acategoryId").text(val.category.name);
       		if(val.address == null || val.address == ""){
       		    $("#aaddressId").text("Not Specified"); 
       		}else{
       		    $("#aaddressId").text(val.address);
       		}
       		if(val.city_id == null || val.city_id == ""){
       		    $("#acityId").text("Not Specified");
       		}else{
			    $("#acityId").text(val.city.name);	
       		}
        }
    });	
}

function acceptScrap(scrapid,btn){	
	var a = confirm('Are you sure to accept this Scrap ?');
	if(a){
		var url = configpath['globalPath'] + 'agents/ajaxacceptscrap';
	    var data = {'id' : scrapid,'agency_id' : loginAgencyObj.id};
	    var datatype = "json";
	    var loaderId = "";
	    var result = requestPostAjax(url, data, datatype, loaderId);
	    if(result.status){
	    	$(btn).text('Accepted');
	    	$(btn).attr('title','Accepted');
	    	$(btn).attr('disabled',true);
	    	$(btn).removeClass('btn-info');
	    	$(btn).addClass('btn-primary');
	    }else{
	    	alert(result.msg);
	    }
	}
}

button = '';
function assignScrap(index,btn){
	button = btn;
	openScrapDetailModalForApproved(index);
	$("#scrapAssignModal").modal('show');
}

function validationForDriverAssign(asdata){
	if(asdata.id.trim() == ''){
		return false;
	}
	if(asdata.driverid.trim() == ''){
		return false;
	}
	return true;
}

function assignDriverAjax(){
    var assigndata = {
    	'id' : $("#ascrapId").val(),
    	'driverid' : $("#assignedDriverId").val()
    };
    if(validationForDriverAssign(assigndata)){
    	var d = confirm('Are you sure to assign this Scrap ?');
		if(d){
			var url = configpath['globalPath'] + 'agents/ajaxassignscrap';
		    var datatype = "json";
		    var loaderId = "";
		    var result = requestPostAjax(url, assigndata, datatype, loaderId);
		    if(result.status){
		    	$(button).text('Assigned');
		    	$(button).attr('title','Assigned');
		    	$(button).attr('disabled',true);
		    	$(button).removeClass('btn-danger');
		    	$(button).addClass('btn-primary');
		    	$("#scrapAssignModal").modal('hide');
		    	$("#ascrapId, #assignedDriverId").val("");
		    }else{
		    	alert(result.msg);	
		    }
		}
    }else{
    	alert('Please Select Driver First');
    }		
}

$(document).ready(function(){
	$("#assignDriverPopBtnId").click(function(){
		assignDriverAjax();
	});	
});