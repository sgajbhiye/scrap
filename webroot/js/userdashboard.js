function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	if(e.total > 8000000){
        		alert('Image size is too large to upload');
        		$("#uploadScrapImgId").val('');
        		$('#showUploadImgId').attr('src', configpath['globalPath'] + 'img/demoscrap.png');
        	}else{
            	$('#showUploadImgId').attr('src', e.target.result);        		
        	}
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function(){
	$("#uploadScrapImgId").change(function(){
		readURL(this);	
	});

	$("#scrapFormResetId").click(function(){
		$('#showUploadImgId').attr('src', configpath['globalPath'] + 'img/demoscrap.png');	
	});
});