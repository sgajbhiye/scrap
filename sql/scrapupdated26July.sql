CREATE TABLE `agency_details` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `agencyname` varchar(100) DEFAULT NULL,
  `typeofagency` varchar(30) DEFAULT NULL,
  `registration` varchar(50) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `agency_details` (`id`, `users_id`, `agencyname`, `typeofagency`, `registration`, `category`, `created`, `modified`, `archived`) VALUES
(1, 13, 'NMC', 'Government', 'N987654FD', '5,8,3,1,2', '2017-07-26 07:58:43', '2017-07-26 07:58:43', 0),
(2, 14, 'Kanak Resource', 'Private', 'K45210J23', '11,5,6,1,2', '2017-07-26 08:04:14', '2017-07-26 08:04:14', 0);

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `archived` tinyint(4) DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `city` (`id`, `name`, `state`, `archived`, `created`, `modified`) VALUES
(1, 'Nagpur', 'MH', 0, '2017-07-22 07:37:54', '2017-07-22 07:37:54'),
(2, 'Raipur', 'CG', 0, '2017-07-22 08:13:53', '2017-07-22 08:13:53'),
(3, 'Wardha', 'MH', 0, '2017-07-22 09:54:05', '2017-07-22 09:54:05');

CREATE TABLE `driver_details` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `agency_details_id` int(11) DEFAULT NULL,
  `drivinglicense` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `driver_details` (`id`, `users_id`, `agency_details_id`, `drivinglicense`, `created`, `modified`, `archived`) VALUES
(1, 15, 1, 'img/DrivingLisence/2017/835443dummy.png', '2017-07-26 08:10:33', '2017-07-26 08:10:33', 0),
(2, 16, 2, 'img/DrivingLisence/2017/612809dummy.png', '2017-07-26 08:16:40', '2017-07-26 08:16:40', 0);

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(60) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role` (`id`, `role`, `created`, `modified`, `archived`) VALUES
(1, 'admin', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0),
(2, 'user', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0),
(3, 'agency', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0),
(4, 'driver', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0);

CREATE TABLE `scrap_category` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `description` text,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `scrap_category` (`id`, `name`, `description`, `created`, `modified`, `archived`) VALUES
(1, 'Muncipal Waste', 'Please Add Description', '2017-07-07 13:04:46', '2017-07-07 13:04:46', 0),
(2, 'Plastic Waste ', 'Please Add Description', '2017-07-07 13:04:57', '2017-07-07 13:04:57', 0),
(3, 'Glass Waste', 'Please Add Description', '2017-07-07 13:05:06', '2017-07-07 13:05:06', 0),
(4, 'Wood Waste', 'Please Add Description this is added', '2017-07-07 13:05:14', '2017-07-07 13:05:14', 1),
(5, 'Chemical Waste', 'Please Add Description', '2017-07-07 13:05:24', '2017-07-07 13:05:24', 0),
(6, 'E - Waste', 'Please Add Description', '2017-07-07 13:05:51', '2017-07-07 13:05:51', 0),
(7, 'Metal Scrap', 'Please Add Description', '2017-07-07 13:05:58', '2017-07-07 13:05:58', 0),
(8, 'Construction Waste', 'Please Add Description', '2017-07-07 13:06:08', '2017-07-07 13:06:08', 0),
(9, 'Industrial Waste', 'Please Add Description', '2017-07-07 13:06:17', '2017-07-07 13:06:17', 0),
(11, 'Cargo Waste', 'Test Description this is test ', '2017-07-11 15:28:22', '2017-07-11 15:28:22', 0);

CREATE TABLE `upload_scraps` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `scrap_category_id` int(11) DEFAULT NULL,
  `agency_details_id` int(11) DEFAULT NULL,
  `driver_details_id` int(11) DEFAULT NULL,
  `scrapimg` text,
  `address` text,
  `lat` varchar(100) DEFAULT NULL,
  `longi` varchar(100) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `description` text,
  `status` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `fname` varchar(60) DEFAULT NULL,
  `lname` varchar(60) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` text,
  `city_id` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `postalcode` varchar(12) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `role_id`, `fname`, `lname`, `gender`, `dob`, `mobile`, `email`, `address`, `city_id`, `state`, `postalcode`, `password`, `created`, `modified`, `archived`) VALUES
(1, 1, 'Sankalp', 'Gajbhiye', 'male', '1990-06-15', '8109847928', 'gajbhiye.sankalp@gmail.com', 'Plot no. 9, Purohit Layout , Ambajhari', '1', 'MH', '440010', '$2y$10$o4WWh5vqogJ7A12JpjXX0.7WNkFcvuhxQunvPjfPGEE.QJKvmDqn.', '2017-07-11 18:19:33', '2017-07-11 18:19:33', 0),
(12, 2, 'Dharmaraj', 'Gaurkar', 'male', '1990-01-26', '7985463210', 'dharmarajgaurkar@webakruti.com', 'Mangal Murti Square, Hingna Road', '1', 'MH', '440010', '$2y$10$Z3D7SHKP2FmGWJGjs.WAGu4XJWrBBQ8igVFni7IXgNvd9hg/YnJT2', '2017-07-26 07:54:34', '2017-07-26 07:54:34', 0),
(13, 3, NULL, NULL, NULL, NULL, '9685471230', 'nmc@gmail.com', 'Civil Lines Near Bhole Petrol Pump', '1', 'MH', NULL, '$2y$10$fFSw.wIlR4.zm4zEdsAAm.R2lyj1w6ecE0RlhDGIg65mH5Zcr6W8.', '2017-07-26 07:58:43', '2017-07-26 07:58:43', 0),
(14, 3, NULL, NULL, NULL, NULL, '7489651254', 'kanakresource@yahoo.com', 'Subhash Nagar , Near Poonam Mall', '2', 'CG', NULL, '$2y$10$e5Tz.UyWRhDSwlLJuftTLOhl7DTP9vGv74IiRsyooHHqHMm31jxoW', '2017-07-26 08:04:14', '2017-07-26 08:04:14', 0),
(15, 4, 'Raju', 'Goyal', 'male', '1988-08-10', '7489651203', 'rajugoyal@gmail.com', 'Gokul Vihar Colony', '3', 'MH', NULL, '$2y$10$dFJOv26zPqG7Cg6iDPiiNeXnCc/HUzATqnR3yWWMVP0cmvErkOmSS', '2017-07-26 08:10:33', '2017-07-26 08:10:33', 0),
(16, 4, 'Vinod', 'Sahu', 'male', '1989-06-01', '968574125', 'vinodsahu@gmail.com', 'Gwaltoli Near Railway Station', '2', 'CG', NULL, '$2y$10$gSN/pD7ZnhPS6AkBWbpMTuMlom9f2ue8V/UZeDCzzGG.o/mwgfiRq', '2017-07-26 08:16:40', '2017-07-26 08:16:40', 0);

ALTER TABLE `agency_details`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `driver_details`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `scrap_category`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `upload_scraps`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `agency_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `driver_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `scrap_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

ALTER TABLE `upload_scraps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;


-- Driver Track Table added on 29 July 2017 -- 
-- --------------------------------------------------------

--
-- Table structure for table `track_driver`
--

CREATE TABLE `track_driver` (
  `id` bigint(20) NOT NULL,
  `upload_scraps_id` int(11) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `status` enum('3','4','5') DEFAULT '3',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `track_driver` (`id`, `upload_scraps_id`, `latitude`, `longitude`, `status`, `created`, `modified`, `archived`) VALUES
(1, 13, '18.5204303', '73.8567437', '3', '2017-07-29 12:33:47', '2017-07-29 12:33:47', 0),
(2, 13, '21.1458004', '79.0881546', '4', '2017-07-29 12:35:20', '2017-07-29 12:35:20', 0),
(3, 13, '18.5204303', '73.8567437', '5', '2017-07-29 14:49:57', '2017-07-29 14:49:57', 0),
(4, 12, '21.1225686', '79.0561746', '3', '2017-07-29 15:52:48', '2017-07-29 15:52:48', 0),
(5, 12, '20.7506155', '78.6088352', '4', '2017-07-29 16:12:30', '2017-07-29 16:12:30', 0),
(6, 12, '20.7506155', '78.6088352', '5', '2017-07-30 07:06:27', '2017-07-30 07:06:27', 0),
(7, 1, '21.1225686', '79.0561746', '3', '2017-07-30 08:44:08', '2017-07-30 08:44:08', 0),
(8, 1, '20.7530533', '78.6031185', '4', '2017-07-31 05:51:25', '2017-07-31 05:51:25', 0),
(9, 24, '21.1225686', '79.0561746', '3', '2017-08-01 15:14:59', '2017-08-01 15:14:59', 0),
(10, 24, '21.1225686', '79.0561746', '4', '2017-08-01 15:26:02', '2017-08-01 15:26:02', 0),
(11, 19, '21.1225686', '79.0561746', '3', '2017-08-01 16:26:39', '2017-08-01 16:26:39', 0),
(12, 21, '22.0694219744', '82.1705054047', '3', '2017-08-02 04:23:35', '2017-08-02 04:23:35', 0),
(13, 20, '22.0697989', '82.1718731', '3', '2017-08-02 04:23:45', '2017-08-02 04:23:45', 0);

ALTER TABLE `track_driver`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `track_driver`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;


-- Column Added on 23 August , 2017 --
ALTER TABLE `upload_scraps` ADD `assigned` DATETIME NULL DEFAULT NULL AFTER `modified`;

-- Column Added on 07 November , 2017 --
ALTER TABLE `users` ADD `provider` VARCHAR(60) NULL DEFAULT NULL AFTER `password`, ADD `provider_id` VARCHAR(255) NULL DEFAULT NULL AFTER `provider`;