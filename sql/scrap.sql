---sql on 12 July --

-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2017 at 06:46 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `agency_details` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `agencyname` varchar(100) DEFAULT NULL,
  `typeofagency` varchar(30) DEFAULT NULL,
  `registration` varchar(50) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO `agency_details` (`id`, `users_id`, `agencyname`, `typeofagency`, `registration`, `category`, `created`, `modified`, `archived`) VALUES
(1, 2, 'Kanak Resource', 'Private', 'K45210J23', '5,1,2', '2017-07-11 12:53:04', '2017-07-11 12:53:04', 0),
(2, 3, 'NMC', 'Government', 'N987654FD', '5,8,9,1', '2017-07-11 13:18:23', '2017-07-11 13:18:23', 0);

CREATE TABLE `driver_details` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `agency_details_id` int(11) DEFAULT NULL,
  `drivinglicense` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `driver_details` (`id`, `users_id`, `agency_details_id`, `drivinglicense`, `created`, `modified`, `archived`) VALUES
(1, 4, 2, 'img/DrivingLisence/2017/598769a.jpg', '2017-07-11 13:51:25', '2017-07-11 13:51:25', 0),
(2, 5, 1, 'img/DrivingLisence/2017/867551dummy.png', '2017-07-11 13:55:25', '2017-07-11 13:55:25', 0);

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(60) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role` (`id`, `role`, `created`, `modified`, `archived`) VALUES
(1, 'admin', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0),
(2, 'user', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0),
(3, 'agency', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0),
(4, 'driver', '2017-07-04 18:30:38', '2017-07-04 18:30:38', 0);


CREATE TABLE `scrap_category` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `description` text,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `scrap_category` (`id`, `name`, `description`, `created`, `modified`, `archived`) VALUES
(1, 'Muncipal Waste', 'Please Add Description', '2017-07-07 13:04:46', '2017-07-07 13:04:46', 0),
(2, 'Plastic Waste ', 'Please Add Description', '2017-07-07 13:04:57', '2017-07-07 13:04:57', 0),
(3, 'Glass Waste', 'Please Add Description', '2017-07-07 13:05:06', '2017-07-07 13:05:06', 0),
(4, 'Wood Waste', 'Please Add Description this is added', '2017-07-07 13:05:14', '2017-07-07 13:05:14', 0),
(5, 'Chemical Waste', 'Please Add Description', '2017-07-07 13:05:24', '2017-07-07 13:05:24', 0),
(6, 'E - Waste', 'Please Add Description', '2017-07-07 13:05:51', '2017-07-07 13:05:51', 0),
(7, 'Metal Scrap', 'Please Add Description', '2017-07-07 13:05:58', '2017-07-07 13:05:58', 0),
(8, 'Construction Waste', 'Please Add Description', '2017-07-07 13:06:08', '2017-07-07 13:06:08', 0),
(9, 'Industrial Waste', 'Please Add Description', '2017-07-07 13:06:17', '2017-07-07 13:06:17', 0),
(11, 'Cargo Waste', 'Test Description this is test ', '2017-07-11 15:28:22', '2017-07-11 15:28:22', 1);

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `fname` varchar(60) DEFAULT NULL,
  `lname` varchar(60) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` text,
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `postalcode` varchar(12) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `archived` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `role_id`, `fname`, `lname`, `gender`, `dob`, `mobile`, `email`, `address`, `city`, `state`, `postalcode`, `password`, `created`, `modified`, `archived`) VALUES
(1, 1, 'Sankalp', 'Gajbhiye', 'male', '1990-06-15', '8109847928', 'gajbhiye.sankalp@gmail.com', 'Nagpur', 'Nagpur', 'MH', '440010', '$2y$10$o4WWh5vqogJ7A12JpjXX0.7WNkFcvuhxQunvPjfPGEE.QJKvmDqn.', '2017-07-11 18:19:33', '2017-07-11 18:19:33', 0),
(2, 3, NULL, NULL, NULL, NULL, '7564123021', 'kanakresource@yahoo.com', 'South Ambajhari Nagpur ', NULL, NULL, NULL, '$2y$10$fS.GLubelOfNqXeTSy0yve3cjdmR9VEGI54TLeptveEylSwIJxcFC', '2017-07-11 12:53:04', '2017-07-11 12:53:04', 0),
(3, 3, NULL, NULL, NULL, NULL, '7896541230', 'nmc@nagpurmail.com', 'Civil Lines Nagpur', NULL, NULL, NULL, '$2y$10$sw2TR43YdDX9rS0ycAgsZOzN5yMnfvrwKeCKCsd2fM6IX/S6bJVQO', '2017-07-11 13:18:23', '2017-07-11 13:18:23', 0),
(4, 4, 'Kumar', 'Thakur', 'male', '1986-07-16', '7896541232', 'kumarthakur@rediffmail.com', 'Hinga Road , Nagpurrrrr', NULL, NULL, NULL, '$2y$10$8Mj/STx7xPNYN6l4joe58.1T2xPgQ6RqAFigwTdY/ShU9breetTzm', '2017-07-11 13:51:25', '2017-07-11 13:51:25', 0),
(5, 4, 'Bablu', 'Patel', 'male', '1988-10-19', '7845693210', 'bablupatel@gmail.com', 'Nagpur', NULL, NULL, NULL, '$2y$10$8qLSUOPMaDvu3gh6jMjG9e/DfsBFsh/G1fRjrfowx8z53fNDi8L2e', '2017-07-11 13:55:25', '2017-07-11 13:55:25', 0);

ALTER TABLE `agency_details`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `driver_details`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `scrap_category`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `agency_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `driver_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `scrap_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

----------------------------------------------------------------------

-- 21 July 2017 --
ALTER TABLE `upload_scraps` ADD `agency_details_id` INT NULL AFTER `scrap_category_id`, ADD `driver_details_id` INT NULL AFTER `agency_details_id`;
ALTER TABLE `users` CHANGE `city` `city_id` VARCHAR(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `upload_scraps` CHANGE `status` `status` ENUM('0','1','2','3','4') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0';
ALTER TABLE `upload_scraps` CHANGE `status` `status` ENUM('0','1','2','3','4','5') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0';
ALTER TABLE `upload_scraps` CHANGE `city` `city_id` INT NULL DEFAULT NULL;



--7 March 2018 --

ALTER TABLE `users` ADD `social_status` TINYINT(1) NULL DEFAULT NULL AFTER `provider_id`;