<div class="clearfix row">
    <div class="col-md-12">
        <div class="col-md-1">
            <form id="limitFormId">
                <?php $allcount = $this->Paginator->counter('{{count}}'); ?>
                <select id="limitId" style="width: 100%;" name="slimit">
                    <option value="10" <?= $slimit == '10' ? ' selected="selected"' : '';?>>10</option>
                    <option value="25" <?= $slimit == '25' ? ' selected="selected"' : '';?>>25</option>
                    <option value="50" <?= $slimit == '50' ? ' selected="selected"' : '';?>>50</option>
                    <option value="100" <?= $slimit == '100' ? ' selected="selected"' : '';?>>100</option>
                    <option value="<?= $allcount ?>" <?= $slimit == $allcount ? ' selected="selected"' : '';?>>All</option>
                </select>                
            </form>
        </div>
        <div class="col-md-3">
            <?= $this->Paginator->counter(
                'Showing {{start}} to {{end}} of {{count}} records'
            ); ?>
        </div>
        <div class="col-md-8">
            <div class="pagination-large">
                <ul class="pagination pull-right" style="margin-top: 0px">
                    <?= $this->Paginator->prev("<< "); ?>
                    <?= $this->Paginator->numbers(); ?>
                    <?= $this->Paginator->next(" >>"); ?>
                </ul>
                <br>
            </div>
        </div>    
    </div>    
</div>