<nav class="navbar navbar-transparent navbar-absolute">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- <a class="navbar-brand" href="">DASHBOARD</a> -->
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
						<i class="material-icons">dashboard</i>
						<p class="hidden-lg hidden-md">Dashboard</p>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="material-icons">notifications</i>
						<span class="notification">5</span>
						<p class="hidden-lg hidden-md">Notifications</p>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Mike John responded to your email</a></li>
						<li><a href="#">You have 5 new tasks</a></li>
						<li><a href="#">You're now friend with Andrew</a></li>
						<li><a href="#">Another Notification</a></li>
						<li><a href="#">Another One</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
					   <i class="material-icons">person</i>
					   <p class="hidden-lg hidden-md">Profile</p>
					</a>
					<ul class="dropdown-menu">
						<li><?= $this->Html->link('Logout', ['controller' => 'users', 'action' => 'logout']) ?></li>
					</ul>
				</li>
			</ul>
			<?php 
				if($loggeduser || $loggeddriver){$welcome = 'Welcome : '. $nameofloggeduser;} 
				if($admin){ $welcome = "Welcome Admin : " . $nameofloggeduser;}
				if($loggedagency){ $welcome = 'Welcome : '. $singleagency['agencyname'];}
			?>
			<form class="navbar-form">
				<div class="form-group" style="width: 25%;">
					<input type="text" class="form-control" value="<?= $welcome ?>" readonly="" style="width: 100%;">
					<span class="material-input"></span>
				</div>
			</form>
		</div>
	</div>
</nav>