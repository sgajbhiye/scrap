<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="message error" onclick="this.classList.add('hidden');"><?= $message ?></div> -->
<div class="alert alert-danger" onclick="this.classList.add('hidden');">
    <button type="button" aria-hidden="true" class="close">×</button>
    <span><b> Error - </b> <?= $message ?></span>
</div>