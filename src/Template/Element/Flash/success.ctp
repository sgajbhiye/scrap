<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="message success" onclick="this.classList.add('hidden')"><?= $message ?></div> -->
<div class="alert alert-success" onclick="this.classList.add('hidden')">
    <button type="button" aria-hidden="true" class="close">×</button>
    <span><b> Success - </b> <?= $message ?></span>
</div>
