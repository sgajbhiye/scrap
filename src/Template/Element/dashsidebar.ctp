<div class="sidebar" data-color="green" data-image="<?= $this->request->webroot ?>img/sidebar-1.jpg">
	<!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
	<div class="logo">
        <?= $this->Html->link('Soilon Energy', ['controller' => 'users', 'action' => 'dashboard'],['class' => 'simple-text', 'title' => 'Dashboard']) ?>
	</div>
	<div class="sidebar-wrapper">
        <ul class="nav">
            <li class="<?php if($this->request->param('action') == 'dashboard'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-web')) . "Dashboard",
                    array('controller' => 'Users', 'action' => 'dashboard'),
                    array('escape' => false)
                );
                ?>
            </li>
            <?php if($admin){ ?> 
            <li class="<?php if($this->request->param('action') == 'alluser' || $this->request->param('action') == 'user' || $this->request->param('action') == 'edituser'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-user-silhouette')) . "User",
                    array('controller' => 'Users', 'action' => 'alluser'),
                    array('escape' => false)
                );
                ?>
            </li>
            <li class="<?php if($this->request->param('action') == 'agency' || $this->request->param('action') == 'agencylist' || $this->request->param('action') == 'edit'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-factory-stock-house')) . "Agency",
                    array('controller' => 'Agents', 'action' => 'agencylist'),
                    array('escape' => false)
                );
                ?>
            </li>
            <li class="<?php if($this->request->param('action') == 'drivers' || $this->request->param('action') == 'driverlist' || $this->request->param('action') == 'editd'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-delivery-man')) . "Driver",
                    array('controller' => 'Drivers', 'action' => 'driverlist'),
                    array('escape' => false)
                );
                ?>
            </li>
            <li class="<?php if($this->request->param('action') == 'category' || $this->request->param('action') == 'editcategory'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-category')) . "Category",
                    array('controller' => 'Users', 'action' => 'category'),
                    array('escape' => false)
                );
                ?>
            </li>
            <li class="<?php if($this->request->param('action') == 'addcity' || $this->request->param('action') == 'editcity' || $this->request->param('action') == 'citylist'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', 'settings', array('class' => 'material-icons')) . "Manage City",
                    array('controller' => 'Users', 'action' => 'citylist'),
                    array('escape' => false)
                );
                ?>
            </li>
            <li class="<?php if($this->request->param('action') == 'alluploads' || $this->request->param('action') == 'allpending' || $this->request->param('action') == 'allassigned' || $this->request->param('action') == 'allinprocess' || $this->request->param('action') == 'allcomplete'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', 'import_export', array('class' => 'material-icons')) . "Uploaded Task",
                    array('controller' => 'Users', 'action' => 'alluploads'),
                    array('escape' => false)
                );
                ?>
            </li><i class="material-icons"></i>
        <?php } ?>
        <?php if($loggedagency){ ?>
            <li class="<?php if($this->request->param('action') == 'tasks' || $this->request->param('action') == 'pending' || $this->request->param('action') == 'inprocess' || $this->request->param('action') == 'complete' || $this->request->param('action') == 'assigned'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-category')) . "Manage Tasks",
                    array('controller' => 'Agents', 'action' => 'tasks'),
                    array('escape' => false)
                );
                ?>
            </li>    
            <li class="<?php if($this->request->param('action') == 'adriverlist' || $this->request->param('action') == 'drivers' || $this->request->param('action') == 'editd'){ echo "active";} ?>">
                <?php
                echo $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'material-icons icon-delivery-man')) . "Manage Driver",
                    array('controller' => 'Agents', 'action' => 'adriverlist'),
                    array('escape' => false)
                );
                ?>
            </li>
        <?php } ?>
        </ul>
	</div>
</div>