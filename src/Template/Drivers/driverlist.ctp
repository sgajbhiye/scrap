<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">      
                    <div class="row">
                        <div class="col-md-11"><h4 class="title">Drivers List</h4></div>
                        <div class="col-md-1">
                            <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', '+', array('class' => 'material-icons icon-delivery-man')),
                                array('controller' => 'Users', 'action' => 'drivers'),
                                array('escape' => false,'title' => 'Add Driver')
                            );
                            ?>                     
                        </div>
                    </div>
                </div>
                <div class="card-content table-responsive">
                    <?= $this->Flash->render() ?>
                    <table id="driverlist" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                    <!-- <table class="table table-hover"> -->
                        <thead class="text-primary">
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Working For</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Gender</th>
                            <th>DOB</th>
                            <th>Email</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php $i = 1; foreach ($drivers as $key => $driver) { ?>
                        <?php $cityname = $driver['user']['city']; ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td class="text-capitalize"><?= $driver['user']['fname'].' '.$driver['user']['lname'] ?></td>
                                <td><?= $driver['user']['mobile'] ?></td>
                                <td class="text-capitalize"><?= $driver['agency']['agencyname'] ?></td>
                                <td class="text-capitalize"><?= $driver['user']['address'] ?></td>
                                <td class="text-capitalize"><?= $cityname['name'] ?></td>
                                <td class="text-capitalize"><?= $driver['user']['gender'] ?></td>
                                <td class="text-capitalize"><?= !empty($driver['user']['dob']) ? $driver['user']['dob']->i18nFormat('dd-MM-YYYY') : 'Not Provided' ?></td>
                                <td><?= $driver['user']['email'] ?></td>                                
                                <td class="td-actions text-center">
                                    <?= $this->Html->link('edit', ['action' => 'editd', $driver->id],['class' => 'btn btn-primary btn-simple btn-xs material-icons', 'title' => 'Edit Driver', 'style' => 'font-size:15px;']) ?>
                                    <?= $this->Form->postLink(
                                        'close',
                                        ['action' => 'delete', $driver->id],
                                        ['confirm' => 'Are you sure to Delete ?','class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove Driver', 'style' => 'font-size:15px;'])
                                    ?>  
                                </td>
                            </tr>                           
                        <?php } ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>