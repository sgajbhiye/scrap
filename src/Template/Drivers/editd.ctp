<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <div class="row">
                            <div class="col-md-11"><h4 class="title">Edit Driver</h4></div>
                            <div class="col-md-1">
                                <?php if($loggedagency){ ?>
                                    <?php
                                    echo $this->Html->link(
                                        $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                        array('controller' => 'Agents', 'action' => 'adriverlist'),
                                        array('escape' => false,'title' => 'All Drivers')
                                    );
                                    ?> 
                                <?php }else{ ?>
                                    <?php
                                    echo $this->Html->link(
                                        $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                        array('controller' => 'Drivers', 'action' => 'driverlist'),
                                        array('escape' => false,'title' => 'All Drivers')
                                    );
                                    ?> 
                                <?php } ?> 
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                    <?= $this->Flash->render() ?>
                        <form method="post" enctype="multipart/form-data">
                        	<div class="row">
                                <div class="col-md-6">
                                    <?php if($loggedagency){ ?>
                                    <label class="control-label">Agency Name</label>
                                    <select name="agency_details_id" class="selectinpt" readonly="">
                                        <option value="<?= $singleagency['id'] ?>" class="inpoptn"> <?= $singleagency['agencyname'] ?></option>  
                                    </select>
                                    <?php }else{ ?>
                                    <div class="dropdown form-group label-floating">
                                        <label class="control-label">Agency Name</label>
                                        <select name="agency_details_id" class="selectinpt" required="" readonly>
                                            <?php foreach ($agency as $id => $agencyname) { ?>
                                                <?php if($agencyname['id'] == $edriver['agency_details_id']){ ?>
                                                <option value="<?= $agencyname['id'] ?>" class="inpoptn"> <?= $agencyname['agencyname'] ?></option>                                               
                                            <?php } } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Upload Driving Licence</label>
										<button class="btn btn-primary pull-right">Upload</button>
										<input type="file" name="lisenceFile" class="form-control">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Fist Name</label>
										<input type="text" name="fname" value="<?= $edriver['user']['fname'] ?>" class="form-control" required="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Last Name</label>
										<input type="text" name="lname" value="<?= $edriver['user']['lname'] ?>" class="form-control" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group radio-div label-floating">
										<label class="radio-label">Gender</label>
										<input type="radio" class="radio-input" name="gender" value="male" <?php if($edriver['user']['gender'] == 'male'){echo "checked=''";} ?> ><span>Male</span>
										<input type="radio" class="radio-input" name="gender" value="female" <?php if($edriver['user']['gender'] == 'female'){echo "checked=''";} ?> ><span>Female</span>
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">DOB</label>
										<input type="text" id="datepicker" value="<?= date('d-m-Y',strtotime($edriver['user']['dob'])) ?>" class="form-control" name="dob" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
									<div class="form-group label-floating">
										<label class="control-label">Mobile No.</label>
										<input type="text" class="form-control" value="<?= $edriver['user']['mobile'] ?>" name="mobile" required="">
									</div>
                                </div>
                                <div class="col-md-8">
									<div class="form-group label-floating">
										<label class="control-label">Address</label>
										<input type="text" name="address" value="<?= $edriver['user']['address'] ?>" class="form-control">
									</div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">City</label>
                                        <select name="city_id" class="selectinpt" id="cityId">
                                            <option></option>
                                            <?php foreach ($cities as $ci => $city) { ?>
                                                <option value="<?= $city['id'] ?>" atr_state="<?= $city['state']?>" <?php if($city['id'] == $edriver['user']['city_id']){echo"selected=''";} ?> ><?= $city['name'] ?></option>                    
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="floatLabDivId" class="form-group label-floating">
                                        <label class="control-label">State</label>
                                        <input type="text" name="state" id="stateId" value="<?= $edriver['user']['state'] ?>" class="form-control" readonly="">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>