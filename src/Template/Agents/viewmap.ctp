<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">
                    <div class="row">
                        <div class="col-md-11"><h4 class="title">Details On Map</h4></div>
                        <div class="col-md-1">
                            <?php echo $this->Html->link(
                                $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                array('controller' => 'Agents', 'action' => 'complete'),
                                array('escape' => false,'title' => 'All Complete')
                            ); ?>
                        </div>
                    </div>
                </div>
                <div class="card-content table-responsive">                    
                    <div id="map" style="width:100%; height:400px; margin:0px;"></div>
                </div>
                <script>
                    var scrapInitlatitude = '<?php echo $scrap->lat; ?>';
                    var scrapInitlongitude = '<?php echo $scrap->longi; ?>';
                    var scrapCompLat = '<?php echo $scrapcomplete->latitude; ?>';
                    var scrapCompLong = '<?php echo $scrapcomplete->longitude; ?>';
                    var username = '<?php echo $scrapcomplete['scrap']['user']->fname ." ". $scrapcomplete['scrap']['user']->lname ?>';
                    var usermobile = '<?php echo $scrapcomplete['scrap']['user']->mobile ?>';
                    var drivername = '<?php echo $scrapcomplete['scrap']['driver']['user']->fname . " " . $scrapcomplete['scrap']['driver']['user']->lname; ?>';
                    var drivermobile = '<?php echo $scrapcomplete['scrap']['driver']['user']->mobile  ?>'
                    function initMap() {
                        var url = 'http://sluhoff.net/wp-content/uploads/2016/11/location_icon.png';
                        var url_driv = 'http://www.clker.com/cliparts/q/w/l/n/r/C/red-driver-marker.svg.med.png';
                        var myLatLng = {lat: parseInt(scrapInitlatitude), lng: parseInt(scrapInitlongitude)};

                        var contentStringsu = '<div id="content">'+                           
                            '<h3 id="firstHeading" class="firstHeading"><b>Scrap User Info</b></h3>'+
                            '<div id="bodyContent">'+
                                '<p><b>Name : </b> '+ username +'<br>' +
                                '<b>Mobile : </b> '+ usermobile +'<br></p>'+
                            '</div>'+
                            '</div>';

                        var contentStringdu = '<div id="content">'+                           
                            '<h3 id="firstHeading" class="firstHeading"><b>Driver Info</b></h3>'+
                            '<div id="bodyContent">'+
                                '<p><b>Name : </b> '+ drivername +' <br>' +
                                '<b>Mobile : </b>  '+ drivermobile +' <br></p>'+
                            '</div>'+
                            '</div>';

                        var infowindowsu = new google.maps.InfoWindow({
                            content: contentStringsu
                        });

                        var infowindowdu = new google.maps.InfoWindow({
                            content: contentStringdu
                        });


                        var map = new google.maps.Map(document.getElementById('map'), {
                            center: myLatLng,
                            zoom: 18,
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            title: 'Details On Map Initial',
                            icon: {
                                url: url,
                                scaledSize: new google.maps.Size(50, 50)
                            }
                        });

                        marker.addListener('click', function() {
                            infowindowsu.open(map, marker);
                        });

                        var marker1 = new google.maps.Marker({
                            position: {lat: parseInt(scrapCompLat), lng: parseInt(scrapCompLong)},
                            map: map,
                            title: 'Details On Map Complete',
                            icon: {
                                url: url_driv,
                                scaledSize: new google.maps.Size(25, 40)
                            }
                        });

                        marker1.addListener('click', function() {
                            infowindowdu.open(map, marker1);
                        });
                    }
                </script>
            </div>
        </div>
    </div>
</div>
