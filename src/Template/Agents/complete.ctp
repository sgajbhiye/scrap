<style>
    .form-g-search{margin: 18px 0 0 0;}
    .form-g-search input{color: #fff;}
    .form-g-search .searchlabel{color: #fff!important;}
    .card .card-header{padding: 4px 15px;}   
</style>
<div class="content">
    <div class="container-fluid">
        <div class="btn-group btn-group-justified" role="group" aria-label="">
            <div class="btn-group" role="group">
                <?= $this->Html->link('New Task', ['controller' => 'Agents', 'action' => 'tasks'],['class' => 'btn btn-info']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Pending', ['controller' => 'Agents', 'action' => 'pending'],['class' => 'btn btn-danger']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Assigned', ['controller' => 'Agents', 'action' => 'assigned'],['class' => 'btn assignedbtnbg']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('In_Process', ['controller' => 'Agents', 'action' => 'inprocess'],['class' => 'btn btn-warning']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Completed', ['controller' => 'Agents', 'action' => 'complete'],['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 style="padding-top: 10px;">Completed Tasks</h4>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-g-search label-floating">
                                        <label class="control-label searchlabel">From Date</label>
                                        <input type="text" id="startDateId" value="<?= isset($startdate) ? date('d-m-Y', strtotime($startdate)) : '' ?>" name="startDate" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-g-search label-floating">
                                        <label class="control-label searchlabel">To Date</label>
                                        <input type="text" id="endDateId" value="<?=isset($enddate) ? date('d-m-Y', strtotime($enddate)) : '' ?>" name="endDate" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                        <i class="material-icons">search</i><div class="ripple-container"></div>
                                    </button>
                                </div>                                
                            </div> 
                        </form>      
                    </div>
                    <div class="card-content table-responsive">                       
                        <table id="completeTasksListId" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                        <!-- <table class="table table-hover"> -->
                            <thead class="text-primary">
                                <th>S.No.</th>
                                <th>Uploaded By</th>
                                <th>Mobile No</th>
                                <th>Email</th>
                                <th>Assigned To</th>
                                <th>Mobile</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Upload Date</th>
                                <th>Complete Date</th>
                                <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                            <?php $i = 1; foreach ($completedtasks as $key => $value) { 
                                $driverdetail = $value['driver']['user'];
                                $fname = !empty($value['user']['fname']) ? $value['user']['fname'] : 'Not Specified';
                                $lname = !empty($value['user']['lname']) ? " ". $value['user']['lname'] : '';
                                $mobile = !empty($value['user']['mobile']) ? " ". $value['user']['mobile'] : '';
                                $email = !empty($value['user']['email']) ? " ". $value['user']['email'] : '';  
                            ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= $fname ." ". $lname ?></td>
                                    <td><?= $mobile ?></td>
                                    <td><?= $email ?></td>
                                    <td><?= $driverdetail['fname'] ." ". $driverdetail['lname'] ?></td>
                                    <td><?= $driverdetail['mobile'] ?></td>
                                    <td><?= $value['category']['name'] ?></td>
                                    <td><?= $value['address'] ?></td>
                                    <td><?= $value['city']['name'] ?></td>
                                    <td><?= $value['created']->i18nFormat('dd-MM-YYY') ?></td>
                                    <td><?= $value['modified']->i18nFormat('dd-MM-YYY') ?></td>
                                    <td class="text-center">
                                        <button type="button" onclick="openScrapDetailModal('<?= $key ?>')" title="Click To View Details"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                        <?= $this->Html->link('', ['action' => 'viewmap', $value->id],['class' => 'btn btn-info btn-sm fa fa-map-marker', 'title' => 'View Map', 'style' => 'font-size:15px;']) ?>
                                    </td>                                    
                                </tr>
                            <?php } ?>    
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
<script type="text/javascript">
    var data = '<?= ($jsoncompletedtasks != "") ? $jsoncompletedtasks : "" ?>';
    if(data.trim() != ""){
        data = JSON.parse(data);
        uploadTaskObj  = [];
        uploadTaskObj  = data;
    }
</script>