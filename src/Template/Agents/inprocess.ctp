<div class="content">
    <div class="container-fluid">
        <div class="btn-group btn-group-justified" role="group" aria-label="">
            <div class="btn-group" role="group">
                <?= $this->Html->link('New Task', ['controller' => 'Agents', 'action' => 'tasks'],['class' => 'btn btn-info']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Pending', ['controller' => 'Agents', 'action' => 'pending'],['class' => 'btn btn-danger']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Assigned', ['controller' => 'Agents', 'action' => 'assigned'],['class' => 'btn assignedbtnbg']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('In_Process', ['controller' => 'Agents', 'action' => 'inprocess'],['class' => 'btn btn-warning']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Completed', ['controller' => 'Agents', 'action' => 'complete'],['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="orange">      
                        <h4 class="title">Tasks In_Process</h4>
                    </div>
                    <div class="card-content table-responsive">
                        <table id="inProcessTasksListId" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                        <!-- <table class="table table-hover"> -->
                            <thead class="text-primary">
                                <th>S.No.</th>
                                <th>Uploaded By</th>
                                <th>Mobile No</th>
                                <th>Email</th>
                                <th>Assigned To</th>
                                <th>Mobile</th>
                                <th>Category</th>
                                <th style="width: 125px;">Address</th>
                                <th>City</th>
                                <th>Upload Date</th>
                                <th>Assign Date</th>
                                <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                            <?php $i = 1; foreach ($inprocesstasks as $key => $value) { 
                                $driverdetail = $value['driver']['user'];
                                $fname = !empty($value['user']['fname']) ? $value['user']['fname'] : 'Not Specified';
                                $lname = !empty($value['user']['lname']) ? " ". $value['user']['lname'] : '';
                                $mobile = !empty($value['user']['mobile']) ? " ". $value['user']['mobile'] : '';
                                $email = !empty($value['user']['email']) ? " ". $value['user']['email'] : '';  
                            ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= $fname ." ". $lname ?></td>
                                    <td><?= $mobile ?></td>
                                    <td><?= $email ?></td>
                                    <td><?= $driverdetail['fname'] ." ". $driverdetail['lname'] ?></td>
                                    <td><?= $driverdetail['mobile'] ?></td>
                                    <td><?= $value['category']['name'] ?></td>
                                    <td><?= $value['address'] ?></td>
                                    <td><?= $value['city']['name'] ?></td>
                                    <td><?= $value['created']->i18nFormat('dd-MM-YYY') ?></td>
                                    <td><?= $value['modified']->i18nFormat('dd-MM-YYY') ?></td>
                                    <td class="text-center"><button type="button" onclick="openScrapDetailModal('<?= $key ?>')" title="Click To View Details"><i class="fa fa-eye" aria-hidden="true"></i></button></td>
                                </tr>
                            <?php } ?>    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
<script type="text/javascript">
    var data = '<?= ($jsoninprocesstasks != "") ? $jsoninprocesstasks : "" ?>';
    if(data.trim() != ""){
        data = JSON.parse(data);
        uploadTaskObj  = [];
        uploadTaskObj  = data;
    }
</script>