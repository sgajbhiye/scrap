<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">      
                    <div class="row">
                        <div class="col-md-11"><h4 class="title">Agency List</h4></div>
                        <div class="col-md-1">
                            <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', '<span class="grencolor">.</span><b>+</b>', array('class' => 'material-icons icon-factory-stock-house')),
                                array('controller' => 'Users', 'action' => 'agency'),
                                array('escape' => false,'title' => 'Add Agency')
                            );
                            ?>                     
                        </div>
                    </div>
                </div>
                <div class="card-content table-responsive">
                    <?= $this->Flash->render() ?>
                    <table id="agencylist" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <!-- <table class="table table-hover"> -->
                        <thead class="text-primary">
                            <th>S.No.</th>
                            <th><?= $this->Paginator->sort('agencyname', 'Agency')?> </th>
                            <th><?= $this->Paginator->sort('typeofagency', 'Type')?> </th>
                            <th><?= $this->Paginator->sort('city_id', 'City')?> </th>
                            <th style="width: 50%;"><?= $this->Paginator->sort('category', 'Category')?> </th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php $i = 1; foreach ($agencies as $u => $user) { ?>
                        <?php $cityname = $user['user']['city']; ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td class="text-capitalize"><?= $user['agencyname'] ?></td>
                                <td><?= $user['typeofagency'] ?></td>
                                <td class="text-capitalize"><?= $cityname['name'] ?></td>
                                <td class="text-capitalize">
                                	<?php 	
			                       		if(!empty($user['category'])){
			                       			$user['category'] = explode(",",$user['category']);
			                       			for ($j=0; $j < count($user['category']); $j++) { 
			                       				foreach ($catgy as $key => $value) {
			                       					if($value['id'] == $user['category'][$j]){
			                       						echo "<span style='color:#9d29b0;'> || </span>". $value['name'];
			                       					}		
			                   					}	
			                   				}	
			                			} 
			                		?>
                                </td>
                                <td class="td-actions text-center">
                                    <?= $this->Html->link('edit', ['action' => 'edit', $user->id],['class' => 'btn btn-primary btn-simple btn-xs material-icons', 'title' => 'Edit Agency', 'style' => 'font-size:15px;']) ?>
                                    <?= $this->Form->postLink(
                                        'close',
                                        ['action' => 'delete', $user->id],
                                        ['confirm' => 'Are you sure to Delete ?','class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove Agency', 'style' => 'font-size:15px;'])
                                    ?> 
                                </td>
                            </tr>                           
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?= $this->element('pagination'); ?>
            </div>
        </div>
    </div>
</div>