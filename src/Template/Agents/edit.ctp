<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <div class="row">
                            <div class="col-md-11"><h4 class="title">Edit Agency</h4></div>
                            <div class="col-md-1">
                                <?php
                                echo $this->Html->link(
                                    $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                    array('controller' => 'Agents', 'action' => 'agencylist'),
                                    array('escape' => false,'title' => 'All Agencies')
                                );
                                ?>  
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                    <?= $this->Flash->render() ?>
                        <form method="post">
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Agency Name</label>
										<input type="text" name="agencyname" class="form-control" value="<?= $edit['agencyname'] ?>" required="">
									</div>
                                </div>
                                <div class="col-md-6 radio">
                                    <label style="padding: 0 0 4px 10px;color:#b9b9b4;">Type Of Agency</label><br>
                                    <label style="color:#4caf50;">
                                        <input type="radio" name="typeofagency" value="Government" <?php if($edit['typeofagency'] == "Government"){ echo "checked=''";} ?>><span class="circle"></span><span class="check"></span> Government
                                    </label>
                                    <label style="color:#4caf50; margin-left: 6%;">
                                        <input type="radio" name="typeofagency" value="Private" <?php if($edit['typeofagency'] == "Private"){ echo "checked=''";} ?>><span class="circle"></span><span class="check"></span> Private
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
									<div class="form-group label-floating">
										<label class="control-label">Mobile No.</label>
										<input type="text" name="mobile" class="form-control" value="<?= $edit['user']['mobile'] ?>" required="">
									</div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">City</label>
                                        <select name="city_id" class="selectinpt" id="cityId">
                                            <option></option>
                                            <?php foreach ($cities as $ci => $city) { ?>
                                                <option value="<?= $city['id'] ?>" atr_state="<?= $city['state']?>" <?php if($city['id'] == $edit['user']['city_id']){echo "selected=''";} ?>><?= $city['name'] ?></option>                    
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div id="floatLabDivId" class="form-group label-floating">
                                        <label class="control-label">State</label>
                                        <input type="text" name="state" id="stateId" value="<?= $edit['user']['state'] ?>" class="form-control" readonly="">
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-8">
									<div class="form-group label-floating">
										<label class="control-label">Address</label>
										<input type="text" name="address" value="<?= $edit['user']['address'] ?>" class="form-control">
									</div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Registration No.</label>
                                        <input type="text" name="registration" value="<?= $edit['registration'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 checkbox">
                                    <?php $edit['category'] = explode(",",$edit['category']); foreach ($catgy as $ke => $valu) { ?>                                    
                                        <label class="rdobtnwidth">
                                            <input type="checkbox" name="category[]" <?php foreach ($edit['category'] as $key => $value) { if($value == $valu['id']){ echo "checked=''";} } ?> value="<?= $valu['id'] ?>"><span class="checkbox-material"></span><?= $valu['name'] ?>
                                        </label>
                                    <?php } ?>                                    
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary pull-right">Update</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>