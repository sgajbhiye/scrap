<div class="content">
    <div class="container-fluid">
        <div class="btn-group btn-group-justified" role="group" aria-label="">
            <div class="btn-group" role="group">
                <?= $this->Html->link('New Task', ['controller' => 'Agents', 'action' => 'tasks'],['class' => 'btn btn-info']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Pending', ['controller' => 'Agents', 'action' => 'pending'],['class' => 'btn btn-danger']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Assigned', ['controller' => 'Agents', 'action' => 'assigned'],['class' => 'btn assignedbtnbg']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('In_Process', ['controller' => 'Agents', 'action' => 'inprocess'],['class' => 'btn btn-warning']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Completed', ['controller' => 'Agents', 'action' => 'complete'],['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="blue">      
                        <h4 class="title">New Tasks</h4>
                    </div>
                    <div class="card-content table-responsive">
                        <?= $this->Flash->render() ?>
                        <table id="newTasksListId" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                        <!-- <table class="table table-hover"> -->
                            <thead class="text-primary">
                                <th>S.No.</th>
                                <th>Details</th>
                                <th>Uploaded By</th>
                                <th>Mobile No</th>
                                <th>Email</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Date</th>
                                <th class="text-center">Status</th>
                            </thead>
                            <tbody>
                                <?php $i = 1; foreach ($tasks as $key => $value) {  
                                    $fname = !empty($value['user']['fname']) ? $value['user']['fname'] : 'Not Specified';
                                    $lname = !empty($value['user']['lname']) ? " ". $value['user']['lname'] : '';
                                    $mobile = !empty($value['user']['mobile']) ? " ". $value['user']['mobile'] : '';
                                    $email = !empty($value['user']['email']) ? " ". $value['user']['email'] : '';
                                ?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><button type="button" class="btn btn-primary btn-sm" onclick="openScrapDetailModal('<?= $key ?>','uploadTaskObj')">View Details</button></td>
                                    <td><?= $fname ." ". $lname ?></td>
                                    <td><?= $mobile ?></td>
                                    <td><?= $email ?></td>
                                    <td><?= $value['category']['name'] ?></td>
                                    <td><?= $value['address'] ?></td>
                                    <td><?= $value['city']['name'] ?></td>
                                    <td><?= $value['created']->i18nFormat('dd-MM-YYY') ?></td>
                                    <td class="text-center"><button type="button" class="btn btn-info" onclick="acceptScrap('<?= $value['id'] ?>',this)" title="Click To Accept">Not Accepted</button></td>
                                </tr>    
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
<script type="text/javascript">
    var data = '<?= ($jsonnewtasks != "") ? $jsonnewtasks : "" ?>';
    var agencydata = '<?= ($jsonagencydetail != "") ? $jsonagencydetail : "" ?>';
    if(data.trim() != ""){
        data = JSON.parse(data);
        uploadTaskObj = [];
        uploadTaskObj = data;
    }
    if(agencydata.trim() != ""){
        agencydata = JSON.parse(agencydata);
        loginAgencyObj = [];
        loginAgencyObj = agencydata;
    }
</script>