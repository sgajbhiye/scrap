<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">      
                    <div class="row">
                        <div class="col-md-11"><h4 class="title">Drivers List</h4></div>
                        <div class="col-md-1">
                            <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', '+', array('class' => 'material-icons icon-delivery-man')),
                                array('controller' => 'Users', 'action' => 'drivers'),
                                array('escape' => false,'title' => 'Add Driver')
                            );
                            ?>                     
                        </div>
                    </div>
                </div>
                <div class="card-content table-responsive">
                    <?= $this->Flash->render() ?>
                    <table id="agencyDriversListId" class="table table-striped" cellspacing="0" width="100%">
                    <!-- <table class="table table-hover"> -->
                        <thead class="text-primary">
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php $i = 1; foreach ($agencydriverslist as $key => $driver) { ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td class="text-capitalize"><?= $driver['user']['fname'].' '.$driver['user']['lname'] ?></td>
                                <td><?= $driver['user']['mobile'] ?></td>
                                <td><?= $driver['user']['email'] ?></td>
                                <td class="text-capitalize"><?= $driver['user']['address'] ?></td>
                                <td class="td-actions text-center">
                                    <?= $this->Html->link('edit', ['controller' => 'Drivers','action' => 'editd', $driver->id],['class' => 'btn btn-primary btn-simple btn-xs material-icons', 'title' => 'Edit Driver', 'style' => 'font-size:15px;']) ?>
                                    <?= $this->Form->postLink(
                                        'close',
                                        ['controller' => 'Drivers','action' => 'delete', $driver->id],
                                        ['confirm' => 'Are you sure to Delete ?','class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove Driver', 'style' => 'font-size:15px;'])
                                    ?>  
                                </td>
                            </tr>                           
                        <?php  } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>