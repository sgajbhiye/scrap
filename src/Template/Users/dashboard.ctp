<div class="content">
	<div class="container-fluid">
		<?php if($admin){ ?> 
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card card-stats">
						<div class="card-header" data-background-color="orange">
							<i class="material-icons icon-user-silhouette"></i>
						</div>
						<div class="card-content">
							<p class="category">Users</p>
							<h3 class="title"><?= $ucount ."+" ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Add User', ['controller' => 'users', 'action' => 'user']) ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card card-stats">
						<div class="card-header" data-background-color="red">
							<i class="material-icons icon-factory-stock-house"></i>
						</div>
						<div class="card-content">
							<p class="category">Agencies</p>
							<h3 class="title"><?= $acount ."+" ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Add Agency', ['controller' => 'users', 'action' => 'agency']) ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card card-stats">
						<div class="card-header" data-background-color="green">
							<i class="material-icons icon-delivery-man"></i>
						</div>
						<div class="card-content">
							<p class="category">Driver</p>
							<h3 class="title"><?= $dcount ."+" ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Add Driver', ['controller' => 'users', 'action' => 'drivers']) ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php }else if($loggeduser){ ?> 
			<div class="col-md-12">
	            <div class="card">
	                <div class="card-header" data-background-color="green">      
	                    <h4 class="title">Upload Material</h4>
	                </div>
	                <div class="card-content">
	                <?= $this->Flash->render() ?>
	                	<?= $this->Form->create(null, ['url' => ['controller' => 'Scraps', 'action' => 'index'], 'enctype' => 'multipart/form-data']); ?>
	                        <div class="row">
	                        	<div class="col-md-4">
	                        		<img src="<?= $this->request->webroot ?>img/demoscrap.png" id="showUploadImgId" style="max-height:300px;" alt="Uploaded_Material" class="img-responsive">	
	                        		<label class="control-label">Note : Maximum Allowed Image Size To Upload ( 8 MB )</label>
	                        		<div class="form-group" style="margin-top: 0;">
										<button class="btn btn-warning">Select Image</button>
										<input type="file" id="uploadScrapImgId" name="uploadedScrap" class="form-control" required="">
									</div>									
	                        	</div>
	                        	<div class="col-md-8">
	                        		<div class="row">
	                        			<div class="col-md-12">
	                        				<div class="form-group label-floating">
		                        				<label class="control-label">Description of Material</label>
		                        				<textarea class="form-control" name="description"></textarea>
		                        			</div>	                        				
	                        			</div>
	                        		</div>
	                        		<div class="row">
	                        			<div class="col-md-8">
	                        				<div class="form-group label-floating">
		                        				<label class="control-label">Address</label>
		                        				<input type="text" class="form-control" name="address" required="">
		                        			</div>	                        				
	                        			</div>
	                        			<div class="col-md-4">
	                        				<div class="form-group label-floating">
	                        				 	<label class="control-label">City</label>
		                                        <select name="city_id" class="selectinpt" id="cityId">
		                                            <option></option>
		                                            <?php foreach ($cities as $ci => $city) { ?>
		                                                <option value="<?= $city['id'] ?>" atr_state="<?= $city['state']?>"><?= $city['name'] ?></option>                    
		                                            <?php } ?>
		                                        </select>
		                        			</div>	                        				
	                        			</div>
	                        		</div>
	                        		<div class="row checkbox">	                        			
	                                    <div class="col-md-12 radio">
	                                    <h5>Select Material Type Or Category</h5>
	                                    <?php foreach ($catgy as $ke => $valu) { ?>
                                            <label class="rdobtnwidth widthupdate">
                                                <input type="radio" name="category" value="<?= $valu['id'] ?>" required=""><span class="circle"></span><span class="check"></span> <?= $valu['name'] ?>
                                            </label>
                                         <?php } ?> 
                                        </div>
	                        		</div>
	                        		<div class="row">
	                        			<div class="col-md-7">
	                        				<button type="reset" id="scrapFormResetId" class="btn btn-info btn-sm" title="Click To Reset Form">Reset</button>
	                        			</div>
	                        			<div class="col-md-4">
	                        				<button type="submit" class="btn btn-primary">Submit</button>
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>
	                    <?= $this->Form->end(); ?>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-12">
	            <div class="card">
	                <div class="card-header" data-background-color="green">      
	                    <h4 class="title">List of Previos Uploads & Status</h4>
	                </div>
	                <div class="card-content table-responsive">
	                    <table class="table table-hover">
	                        <thead class="text-primary">
	                            <th>S.No.</th>
	                            <th>Material Type</th>
	                            <th>Description</th>
	                            <th>Address</th>
	                            <th>Post Date</th>
	                            <th class="text-center">Status</th>
	                        </thead>
	                        <tbody>
	                            <tr>
	                                <td>1</td>
	                                <td>Muncipal Waste</td>
	                                <td>Waste Old than two week</td>
	                                <td>Madhav Nagar</td>
	                                <td>08/07/2017</td>
	                                <td class="success text-center">Done</td>
	                            </tr>
	                            <tr>
	                                <td>2</td>
	                                <td>Plastic Waste </td>
	                                <td>Waste Old than 4 week</td>
	                                <td>Manish Nagar</td>
	                                <td>01/07/2017</td>
	                                <td class="danger text-center">Pending</td>
	                            </tr>
	                            <tr>
	                                <td>3</td>
	                                <td>Construction Waste</td>
	                                <td>Waste Old than one week</td>
	                                <td>Subhash Nagar</td>
	                                <td>28/06/2017</td>
	                                <td class="success text-center">Done</td>
	                            </tr>   
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
		<?php }else if($loggedagency){ ?>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6">
			
					<div class="card card-stats">
						<div class="card-header" data-background-color="blue">
							<i class="material-icons">content_copy</i>
						</div>
						<div class="card-content">
							<p class="category">New Task</p>
							<h3 class="title"><?= $anewtaskcount ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Details', ['controller' => 'agents', 'action' => 'tasks']) ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="card card-stats">
						<div class="card-header" data-background-color="red">
							<i class="material-icons">info_outline</i>
						</div>
						<div class="card-content">
							<p class="category">Pending Tasks</p>
							<h3 class="title"><?= $aaccepttaskcount ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Details', ['controller' => 'agents', 'action' => 'pending']) ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="card card-stats">
						<div class="card-header" data-background-color="green">
							<i class="material-icons">check_circle</i>
						</div>
						<div class="card-content">
							<p class="category">Tasks Done</p>
							<h3 class="title"><?= $adonetaskcount ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Details', ['controller' => 'agents', 'action' => 'complete']) ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="card card-stats">
						<div class="card-header" data-background-color="orange">
							<i class="material-icons icon-delivery-man"></i>
						</div>
						<div class="card-content">
							<p class="category">Driver</p>
							<h3 class="title"><?= $adrivercount ?></h3>
						</div>
						<div class="card-footer">
							<div class="stats">
								<p class="category"><?= $this->Html->link('Details', ['controller' => 'agents', 'action' => 'adriverlist']) ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php }else if($loggeddriver){ ?> 
			<div class="col-md-12">
	            <div class="card">
	                <div class="card-header" data-background-color="green">      
	                    <h4 class="title">Welcome Driver</h4>
	                </div>
	                <div class="card-content">
	                <?= $this->Flash->render() ?>
	                	<form method="post">
	                        <div class="row">
	                        	<div class="col-md-12">
	                        		<p>Working on view for Driver Dashboard, Sorry for the inconvenience</p>                        		
	                        	</div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
		<?php } ?> 
	</div>
</div>
