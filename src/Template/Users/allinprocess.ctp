<div class="content">
    <div class="container-fluid">
        <div class="btn-group btn-group-justified" role="group" aria-label="">
            <div class="btn-group" role="group">
                <?= $this->Html->link('New Task', ['controller' => 'Users', 'action' => 'alluploads'],['class' => 'btn btn-info']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Pending', ['controller' => 'Users', 'action' => 'allpending'],['class' => 'btn btn-danger']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Assigned', ['controller' => 'Users', 'action' => 'allassigned'],['class' => 'btn assignedbtnbg']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('In_Process', ['controller' => 'Users', 'action' => 'allinprocess'],['class' => 'btn btn-warning']) ?>
            </div>
            <div class="btn-group" role="group">
                <?= $this->Html->link('Completed', ['controller' => 'Users', 'action' => 'allcomplete'],['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="orange">      
                        <h4 class="title">Inprocess Tasks</h4>
                    </div>
                    <div class="card-content table-responsive">
                        <?= $this->Flash->render() ?>
                        <table id="exampleuser" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                            <thead class="text-primary">
                                <th>S.No.</th>
                                <th>Uploaded By</th>
                                <th>Mobile No</th>
                                <th>Email</th>
                                <th>Accepted By</th>
                                <th>Agency <i class="fa fa-phone" aria-hidden="true"></i></th>
                                <th>Assigned To</th>
                                <th>Driver <i class="fa fa-phone" aria-hidden="true"></i></th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Date</th>
                                <th>Details</th>
                            </thead>
                            <tbody>
                                <?php $i = 1; foreach ($allinprocesstask as $key => $value) { ?>
                                    <?php
                                        $desc = !empty($value['description']) ? $value['description'] : 'Not Specified';
                                        $fname = !empty($value['user']['fname']) ? $value['user']['fname'] : 'Not Specified';
                                        $lname = !empty($value['user']['lname']) ? " ". $value['user']['lname'] : '';
                                        $mobile = !empty($value['user']['mobile']) ? " ". $value['user']['mobile'] : '';
                                        $category = !empty($value['category']['name']) ? " ". $value['category']['name'] : '';
                                        $address = !empty($value['user']['address']) ? " ". $value['user']['address'] : '';
                                        $city = !empty($value['city']['name']) ? " ". $value['city']['name'] : '';
                                        $imgpath = !empty($value['scrapimg']) ? " ". $value['scrapimg'] : '';
                                        $agencyuser = $value['agency']['user'];
                                        $driveruser = $value['driver']['user'];
                                        $email = !empty($value['user']['email']) ? " ". $value['user']['email'] : '';
                                    ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $fname ." ". $lname ?></td>
                                        <td><?= $mobile ?></td>
                                        <td><?= $email ?></td>
                                        <td><?= $value['agency']['agencyname'] ?></td>
                                        <td><?= $agencyuser['mobile'] ?></td>
                                        <td><?= $driveruser['fname'] ." ". $driveruser['lname'] ?></td>
                                        <td><?= $driveruser['mobile'] ?></td>
                                        <td><?= $value['category']['name'] ?></td>
                                        <td><?= $value['address'] ?></td>
                                        <td><?= $value['city']['name'] ?></td>
                                        <td><?= !empty($value['created']) ? $value['created']->i18nFormat('dd-MM-YYY') : "" ?></td>
                                        <td style="padding: 2px 12px;"><button type="button" class="btn btn-primary btn-sm" onclick='openDetailsModalForAdmin("<?= $desc ?>","<?= $fname . $lname ?>","<?= $mobile ?>","<?= $category ?>","<?= $address ?>","<?= $city ?>","<?= $imgpath ?>")'>View Details</button></td>
                                    </tr>    
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>