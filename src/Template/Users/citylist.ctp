<div class="content">
	<div class="container-fluid">        
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header" data-background-color="green">
	                <div class="row">
                        <div class="col-md-11"><h4 class="title">City List</h4></div>
                        <div class="col-md-1">
                            <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', 'plus_one', array('class' => 'material-icons')),
                                array('controller' => 'Users', 'action' => 'addcity'),
                                array('escape' => false,'title' => 'Add City')
                            );
                            ?>                     
                        </div>
                    </div>
	            </div>
	            <div class="card-content table-responsive">
                	<?= $this->Flash->render() ?>
	                <table id="categorylist" class="table table-hover table-bordered" cellspacing="0" width="100%">
	                    <thead class="text-primary">
	                        <th>Serial No.</th>
	                    	<th>City Name</th>
	                    	<th>State Name</th>
	                    	<th class="text-center">Action</th>
	                    </thead>
	                    <tbody>
	                    <?php $i = 1; foreach ($cities as $k => $val) { ?>
	                        <tr>
	                        	<td><?= $i++ ?></td>
	                        	<td><?= $val['name'] ?></td>
	                        	<td><?= $val['state'] ?></td>
	                        	<td class="td-actions text-center">
									<?= $this->Html->link('edit', ['action' => 'editcity', $val->id],['class' => 'btn btn-primary btn-simple btn-xs material-icons', 'title' => 'Edit Driver', 'style' => 'font-size:15px;']) ?>
                                    <?= $this->Form->postLink(
                                        'close',
                                        ['action' => 'deletecity', $val->id],
                                        ['confirm' => 'Are you sure to Delete ?','class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove Driver', 'style' => 'font-size:15px;'])
                                    ?>  
								</td>
	                        </tr>	                    	
	                    <?php } ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>