<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <div class="row">
                            <div class="col-md-11"><h4 class="title">Add User</h4></div>
                            <div class="col-md-1">
                                <?php
                                echo $this->Html->link(
                                    $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                    array('controller' => 'Users', 'action' => 'alluser'),
                                    array('escape' => false,'title' => 'All Users')
                                );
                                ?>                        
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                    <?= $this->Flash->render() ?>
                        <form method="post" onsubmit="return checkPassword();">
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Fist Name</label>
										<input type="text" name="fname" class="form-control" required="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Last Name</label>
										<input type="text" name="lname" class="form-control" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group radio-div label-floating">
										<label class="radio-label">Gender</label>
										<input type="radio" class="radio-input" name="gender" value="male" checked=""><span>Male</span>
										<input type="radio" class="radio-input" name="gender" value="female"><span>Female</span>
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">DOB</label>
										<input type="text" id="datepicker" class="form-control" name="dob" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Mobile No.</label>
										<input type="text" class="form-control" name="mobile" required="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Email</label>
										<input type="email" class="form-control" name="email" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Password</label>
                                        <input type="password" class="form-control" id="passwordId" name="password" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpasswordId" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Address</label>
										<textarea class="form-control" name="address" required=""></textarea>
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">City</label>
                                        <select name="city_id" class="selectinpt" id="cityId">
                                            <option></option>
                                            <?php foreach ($cities as $ci => $city) { ?>
                                                <option value="<?= $city['id'] ?>" atr_state="<?= $city['state']?>"><?= $city['name'] ?></option>                    
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div id="floatLabDivId" class="form-group label-floating">
                                        <label class="control-label">State</label>
                                        <input type="text" name="state" id="stateId" class="form-control" readonly="">
                                    </div>
                                </div>
                                <div class="col-md-4">
									<div class="form-group label-floating">
										<label class="control-label">Postal Code</label>
										<input type="text" class="form-control" name="postalcode">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2"><label class="control-label">User Role</label></div>
                                <div class="col-md-2">
                                    <select name="role_id" class="selectinpt" required="">
                                        <option value="2" class="inpoptn" selected="">User</option>                                                
                                        <option value="1" class="inpoptn">Admin</option>                                                
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">Register</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>