<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">
                    <div class="row">
                        <div class="col-md-11"><h4 class="title">Users List</h4></div>
                        <div class="col-md-1">
                            <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', 'person_add', array('class' => 'material-icons')),
                                array('controller' => 'Users', 'action' => 'user'),
                                array('escape' => false,'title' => 'Add User')
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="card-content table-responsive">
                    <?= $this->Flash->render() ?>
                    <table id="exampleuser" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                    <!-- <table class="table table-hover"> -->
                        <thead class="text-primary">
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Gender</th>
                            <th>DOB</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php $i = 1; foreach ($userslist as $u => $user) { ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td class="text-capitalize"><?= $user['fname'] ." ". $user['lname'] ?></td>
                                <td><?= $user['mobile'] ?></td>
                                <td><?= $user['email'] ?></td>
                                <td class="text-capitalize"><?= $user['address'] ?></td>
                                <td class="text-capitalize"><?= $user['gender'] ?></td>
                                <td class="text-capitalize"><?= !empty($user['dob']) ? $user['dob']->i18nFormat('dd-MM-YYYY') : 'Not Provided' ?></td>
                                <td class="td-actions text-center">
                                    <?= $this->Html->link('edit', ['action' => 'edituser', $user->id],['class' => 'btn btn-primary btn-simple btn-xs material-icons', 'title' => 'Edit User', 'style' => 'font-size:15px;']) ?>
                                    <?= $this->Form->postLink(
                                        'close',
                                        ['action' => 'deleteuser', $user->id],
                                        ['confirm' => 'Are you sure to Delete ?','class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove User', 'style' => 'font-size:15px;'])
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- <?= $this->Html->link('close', ['action' => 'deleteuser', $user->id],['class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove User', 'style' => 'font-size:15px;']) ?> -->
