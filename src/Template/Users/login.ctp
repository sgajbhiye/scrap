<div class="wrapper">
    <div class="main-panel login-div col-md-12">
        <div class="content">
        <h2 class="text-center">SOILON ENERGY</h2>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="card">
                            <div class="card-header" data-background-color="green">
                                <h4 class="title">Sign In</h4>
                            </div>
                            <div class="card-content">
                            <?= $this->Flash->render() ?>
                                <form method="post">
                                    <div class="row">
                                        <div class="col-md-12">
											<div class="form-group label-floating">
												<label class="control-label">User Name</label>
												<input type="text" name="username" class="form-control" required="">
											</div>
                                        </div>
                                        <div class="col-md-12">
											<div class="form-group label-floating">
												<label class="control-label">Password</label>
												<input type="password" name="password" class="form-control" required="">
											</div>
                                        </div>
                                    </div>
                                    <?= $this->Html->link('Forgot Password ?', ['controller' => 'users', 'action' => 'dforgotpassword'],['class' => 'btn-link pull-left', 'style' => 'padding: 32px 0 0;']) ?>                                    
                                    <button type="submit" class="btn btn-primary pull-right">Sign In</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>