<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">
                    <div class="row">
                        <div class="col-md-11"><h4 class="title">Add City</h4></div>
                        <div class="col-md-1">
                            <?php
                            echo $this->Html->link(
                                $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                array('controller' => 'Users', 'action' => 'citylist'),
                                array('escape' => false,'title' => 'City List')
                            );
                            ?>                     
                        </div>
                    </div>   
                </div>
                <div class="card-content">
                <?= $this->Flash->render() ?>
                    <form method="post">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
        						<div class="form-group label-floating">
        							<label class="control-label">City Name</label>
        							<input type="text" name="name" class="form-control" required="">
        						</div>                                
        						<div class="form-group label-floating">
        							<label class="control-label">State Name</label>
        							<input type="text" name="state" class="form-control" required="">
        						</div>
                        		<button type="submit" class="btn btn-primary text-center">Add</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>