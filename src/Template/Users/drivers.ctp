<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <div class="row">
                            <div class="col-md-11"><h4 class="title">Add Driver</h4></div>
                            <div class="col-md-1">
                                <?php if($loggedagency){ ?>
                                    <?php
                                    echo $this->Html->link(
                                        $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                        array('controller' => 'Agents', 'action' => 'adriverlist'),
                                        array('escape' => false,'title' => 'All Drivers')
                                    );
                                    ?> 
                                <?php }else{ ?>
                                    <?php
                                    echo $this->Html->link(
                                        $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                        array('controller' => 'Drivers', 'action' => 'driverlist'),
                                        array('escape' => false,'title' => 'All Drivers')
                                    );
                                    ?> 
                                <?php } ?> 
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                    <?= $this->Flash->render() ?>
                        <form method="post" enctype="multipart/form-data" onsubmit="return checkPassword();">
                        	<div class="row">
                                <div class="col-md-6">
									<div class="dropdown form-group label-floating">
                                        <?php if($loggedagency){ ?>
                                            <label class="control-label">Agency Name</label>
                                            <select name="agency_details_id" class="selectinpt" readonly="">
                                                <?php foreach ($agency as $id => $agencyname) { if($agencyname['id'] == $singleagency['id']){ ?>
                                                    <option value="<?= $agencyname['id'] ?>" class="inpoptn"> <?= $agencyname['agencyname'] ?></option>                                             
                                                <?php } } ?>
                                            </select>
                                        <?php }else{ ?>
    									    <label class="control-label">Select Agency</label>
    									    <select name="agency_details_id" class="selectinpt" required="">
    									    	<?php foreach ($agency as $id => $agencyname) { ?>
    									    		<option value="<?= $agencyname['id'] ?>" class="inpoptn"> <?= $agencyname['agencyname'] ?></option>									    		
    									    	<?php } ?>
    									    </select>
                                        <?php } ?>
                                        <input type="hidden" name="role_id" value="4">
									    <!-- <input type="text" class="form-control" data-toggle="dropdown" id="navbarDropdownMenuLink1">
									    <ul class="dropdown-menu col-xs-12" aria-labelledby="navbarDropdownMenuLink1">
									        <a class="dropdown-item" href="#">Action</a>
									        <a class="dropdown-item" href="#">Another action</a>
									        <a class="dropdown-item" href="#">Something else here</a>
									        <div class="dropdown-divider"></div>
									        <a class="dropdown-item" href="#">Separated link</a>
									        <div class="dropdown-divider"></div>
									        <a class="dropdown-item" href="#">One more separated link</a>
									    </ul> -->
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Upload Driving Licence</label>
										<button class="btn btn-primary pull-right">Upload</button>
										<input type="file" name="lisenceFile" class="form-control">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Fist Name</label>
										<input type="text" name="fname" class="form-control" required="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Last Name</label>
										<input type="text" name="lname" class="form-control" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group radio-div label-floating">
										<label class="radio-label">Gender</label>
										<input type="radio" class="radio-input" name="gender" value="male" checked=""><span>Male</span>
										<input type="radio" class="radio-input" name="gender" value="female"><span>Female</span>
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">DOB</label>
										<input type="text" id="datepicker" class="form-control" name="dob" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Mobile No.</label>
										<input type="text" class="form-control" name="mobile" required="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Email</label>
										<input type="email" class="form-control" name="email" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Password</label>
                                        <input type="password" class="form-control" name="password" id="passwordId" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpasswordId" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Address</label>
										<input type="text" name="address" class="form-control">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">City</label>
                                        <select name="city_id" class="selectinpt" id="cityId">
                                            <option></option>
                                            <?php foreach ($cities as $ci => $city) { ?>
                                                <option value="<?= $city['id'] ?>" atr_state="<?= $city['state']?>"><?= $city['name'] ?></option>                    
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="floatLabDivId" class="form-group label-floating">
                                        <label class="control-label">State</label>
                                        <input type="text" name="state" id="stateId" class="form-control" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox pull-left">
								<label>
									<input type="checkbox" name="optionsCheckboxes" required=""><span class="checkbox-material"></span>Terms & Condition
								</label> 
							</div>
                            <button type="submit" class="btn btn-primary pull-right">Register</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>