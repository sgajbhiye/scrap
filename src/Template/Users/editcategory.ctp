<div class="content">
	<div class="container-fluid">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="card-header" data-background-color="green">
                	<div class="row">
                		<div class="col-md-11"><h4 class="title">Edit Catgory</h4></div>
                		<div class="col-md-1">
                			<?php
			                echo $this->Html->link(
			                    $this->Html->tag('i', 'list', array('class' => 'material-icons')),
			                    array('controller' => 'Users', 'action' => 'category'),
			                    array('escape' => false)
			                );
			                ?>
                		</div>
                	</div>
                </div>
                <div class="card-content">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Catgory Name</label>
									<input type="text" name="name" value="<?= $editcat['name'] ?>" class="form-control" required="">
								</div>
                            </div>
                            <div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Description</label>
									<textarea class="form-control" rows="1" name="description" required=""><?= $editcat['description'] ?></textarea>
								</div>
                            </div>
                            <div class="col-md-12">
                        		<button type="submit" class="btn btn-primary pull-right">Update</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
	</div>
</div>