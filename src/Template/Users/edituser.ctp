<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <div class="row">
                            <div class="col-md-11"><h4 class="title">Edit User</h4></div>
                            <div class="col-md-1">
                                <?php
                                echo $this->Html->link(
                                    $this->Html->tag('i', 'list', array('class' => 'material-icons')),
                                    array('controller' => 'Users', 'action' => 'alluser'),
                                    array('escape' => false,'title' => 'All Users')
                                );
                                ?>                        
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Fist Name</label>
										<input type="text" name="fname" class="form-control" value="<?= $edituser['fname'] ?>" required="">
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Last Name</label>
										<input type="text" name="lname" class="form-control" value="<?= $edituser['lname'] ?>" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group radio-div label-floating">
										<label class="radio-label">Gender</label>
										<input type="radio" class="radio-input" name="gender" value="male" <?php if($edituser['gender'] == 'male'){echo "checked";} ?>><span>Male</span>
										<input type="radio" class="radio-input" name="gender" value="female" <?php if($edituser['gender'] == 'female'){echo "checked";} ?>><span>Female</span>
									</div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">DOB</label>
										<input type="text" id="datepicker" class="form-control" name="dob" value="<?= date('d-m-Y',strtotime($edituser['dob'])) ?>" required="">
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Mobile No.</label>
										<input type="text" class="form-control" name="mobile" value="<?= $edituser['mobile'] ?>" required="">
									</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">City</label>
                                        <select name="city_id" class="selectinpt" id="cityId">
                                            <option></option>
                                            <?php foreach ($cities as $ci => $city) { ?>
                                                <option value="<?= $city['id'] ?>" atr_state="<?= $city['state']?>" <?php if($city['id'] == $edituser['city_id']){echo "selected=''";} ?>><?= $city['name'] ?></option>                    
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Address</label>
										<textarea class="form-control" name="address" required=""><?= $edituser['address'] ?></textarea>
									</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="floatLabDivId" class="form-group label-floating">
                                        <label class="control-label">State</label>
                                        <input type="text" name="state" id="stateId" value="<?= $edituser['state'] ?>" class="form-control" readonly="">
                                    </div>
                                </div>
                                <div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Postal Code</label>
										<input type="text" class="form-control" value="<?= $edituser['postalcode'] ?>" name="postalcode">
									</div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>