<div class="content">
	<div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="green">
                    <h4 class="title">Add Catgory</h4>
                </div>
                <div class="card-content">
                    <form method="post">
                        <div class="row">
                            <div class="col-md-5">
								<div class="form-group label-floating">
									<label class="control-label">Catgory Name</label>
									<input type="text" name="name" class="form-control" required="">
								</div>
                            </div>
                            <div class="col-md-5">
								<div class="form-group label-floating">
									<label class="control-label">Description</label>
									<textarea class="form-control" rows="1" name="description" required=""></textarea>
								</div>
                            </div>
                            <div class="col-md-2">
                        		<button type="submit" class="btn btn-primary pull-right">Add</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
	    <div class="col-md-12">
	    <?= $this->Flash->render() ?>
	        <div class="card">
	            <div class="card-header" data-background-color="green">
	                <h4 class="title">Category List</h4>
	            </div>
	            <div class="card-content table-responsive">
	             	<table id="categorylist" class="table table-striped table-bordered" cellspacing="0" width="100%">
	                <!-- <table class="table table-hover"> -->
	                    <thead class="text-primary">
	                        <th>Serial No.</th>
	                    	<th>Name</th>
	                    	<th>Description</th>
	                    	<th class="text-center">Action</th>
	                    </thead>
	                    <tbody>
	                    <?php $i = 1; foreach ($catgy as $k => $val) { ?>
	                        <tr>
	                        	<td><?= $i++ ?></td>
	                        	<td><?= $val['name'] ?></td>
	                        	<td><?= $val['description'] ?></td>
	                        	<td class="td-actions text-center">
									<?= $this->Html->link('edit', ['action' => 'editcategory', $val->id],['class' => 'btn btn-primary btn-simple btn-xs material-icons', 'title' => 'Edit Driver', 'style' => 'font-size:15px;']) ?>
                                    <?= $this->Form->postLink(
                                        'close',
                                        ['action' => 'deletecategory', $val->id],
                                        ['confirm' => 'Are you sure to Delete ?','class' => 'btn btn-danger btn-simple btn-xs material-icons', 'title' => 'Remove Driver', 'style' => 'font-size:15px;'])
                                    ?>  
								</td>
	                        </tr>	                    	
	                    <?php } ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>