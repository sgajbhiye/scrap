<?php 
    namespace App\Model\Table;
    use Cake\ORM\Query;
    use Cake\ORM\RulesChecker;
    use Cake\ORM\Table;
    use Cake\Validation\Validator;
    use Cake\Event\Event;
    use ArrayObject;
    
    class TrackingsTable extends Table{
        public function initialize(array $config){
            $this->table('track_driver');
            parent::initialize($config);
    
            $this->belongsTo('Scraps', [
                'className' => 'Scraps',
                'foreignKey' => 'upload_scraps_id'
            ]);
        }

        public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options){
            foreach ($data as $key => $value) {
                $data[$key] = preg_replace('/\s+/S', " ", $value);
                $data[$key] = is_string ($value) ? trim($value) : $value;
            }
        }
    }
?>