<?php
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

class ScrapsTable extends Table{
    public function initialize(array $config){
        $this->table('upload_scraps');
        parent::initialize($config);

        $this->belongsTo('Categorys', [
            'className' => 'Categorys',
            'foreignKey' => 'scrap_category_id'
        ]);

        $this->belongsTo('Drivers', [
            'className' => 'Drivers',
            'foreignKey' => 'driver_details_id'
        ]);
        
        $this->belongsTo('Agencys', [
            'className' => 'Agencys',
            'foreignKey' => 'agency_details_id'
        ]);

        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'users_id'
        ]);

        $this->belongsTo('Cities', [
            'className' => 'Cities',
            'foreignKey' => 'city_id'
        ]);
        
        $this->hasMany('Trackings', [
            'className' => 'Trackings',
            'foreignKey' => 'upload_scraps_id'
        ]);
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options){
        foreach ($data as $key => $value) {
            $data[$key] = preg_replace('/\s+/S', " ", $value);
            $data[$key] = is_string ($value) ? trim($value) : $value;
        }
    }
}
