<?php
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

class UsersTable extends Table{
    public function initialize(array $config){
        parent::initialize($config);

        $this->hasOne('Agencys', [
            'className' => 'Agencys',
            'foreignKey' => 'users_id'
        ]);

        $this->hasOne('Drivers', [
            'className' => 'Drivers',
            'foreignKey' => 'users_id'
        ]);

        $this->hasMany('Scraps', [
            'className' => 'Scraps',
            'foreignKey' => 'users_id'
        ]);

        $this->belongsTo('Cities', [
            'className' => 'Cities',
            'foreignKey' => 'city_id'
        ]);
    }

    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options){
        foreach ($data as $key => $value) {
            $data[$key] = preg_replace('/\s+/S', " ", $value);
            $data[$key] = is_string ($value) ? trim($value) : $value;
        }
    }
}