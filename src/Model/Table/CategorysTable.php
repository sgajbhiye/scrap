<?php
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

class CategorysTable extends Table{
    public function initialize(array $config){
        $this->table('scrap_category');
        parent::initialize($config);
        // $this->belongsTo('UserTypes', [
        //     'className' => 'UserTypes',
        //     'foreignKey' => 'type_id'
        // ]);
        
        // $this->setTable('users');
        // $this->setDisplayField('id');
        // $this->setPrimaryKey('id');
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options){
        foreach ($data as $key => $value) {
            $data[$key] = preg_replace('/\s+/S', " ", $value);
            $data[$key] = is_string ($value) ? trim($value) : $value;
        }
    }
}
