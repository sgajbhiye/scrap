<?php
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

class DriversTable extends Table{
    public function initialize(array $config){
        $this->table('driver_details');
        parent::initialize($config);

        $this->belongsTo('Agencys', [
            'className' => 'Agencys',
            'foreignKey' => 'agency_details_id'
        ]);

        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'users_id'
        ]);
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options){
        foreach ($data as $key => $value) {
            $data[$key] = preg_replace('/\s+/S', " ", $value);
            $data[$key] = is_string ($value) ? trim($value) : $value;
        }
    }
}
