<?php
    namespace App\Controller;

    use App\Controller\AppController;
    use Cake\Event\Event;
    use Cake\Utility\Security;
    use Cake\Routing\Router;
    use Cake\View\Helper\HtmlHelper;
    use Cake\Validation\Validation;
    use Cake\View\View;
    use View\Helper\FormHelper;
    use Cake\Mailer\MailerAwareTrait;
    use Cake\I18nDate;
    use Cake\Utility\Text;  
    class DriversController extends AppController{
        public function initialize(){
            parent::initialize();
            $this->viewBuilder()->layout('AdminManager.dashboardlayout');
            $this->loadComponent('RequestHandler');
            $this->loadModel('Users');
            $this->loadModel('Agencys');
            $this->loadModel('Drivers');
            $this->loadModel('Categorys');
            $this->loadModel('Cities');
            $this->loadModel('Trackings');
        }

        public function beforeFilter(Event $event){
            parent::beforeFilter($event);
        }

        public function driverlist(){
        	if($this->_isAdmin()){
	            $drivers = $this->Drivers->find('all')
	            						->contain(['Users' => [
	            								'fields' => [
	            									'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address','Users.gender','Users.dob'
	            								]
	            							],'Agencys','Users.Cities'])
	                                    ->where(['Drivers.archived' => 0])
	                                    ->order(['Users.fname' => 'ASC']); 
	            $this->set('drivers',$drivers);
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }

        public function editd($id = null){
        	if($this->_isAdmin() || $this->_isAgency()){
	            $drivers = $this->Drivers->find()
	            						->contain(['Users' => [
	            								'fields' => [
	            									'Users.id','Users.fname','Users.lname','Users.mobile','Users.dob','Users.address','Users.gender','Users.city_id','Users.state'
	            								]
	            							],'Agencys'])
	            						->where(['Drivers.id' => $id])
	            						->first();
	            if ($this->request->is(['post', 'put'])) {
	            	$user = $this->Users->get($drivers['users_id']);
	            	$this->request->data['dob'] = date('Y-m-d',strtotime($this->request->data['dob']));
	            	$user = $this->Users->patchEntity($user, $this->request->getdata());
	            	if($this->Users->save($user)){
	            		$driver = $this->Drivers->get($id);
	            		if(!empty($this->request->data['lisenceFile']['tmp_name']) && !empty($this->request->data['lisenceFile']['name'])){
	            			$this->request->data['drivinglicense'] = $this->imageupload($this->request->data['lisenceFile'], 'DrivingLisence');
	            		}
		                $this->Drivers->patchEntity($driver, $this->request->getData());
		                if ($this->Drivers->save($driver)) {
		                    $this->Flash->success(__('Driver Details has been updated.'));
	                     	if($this->_isAgency()){
			                	return $this->redirect(['controller' => 'Agents','action' => 'adriverlist']);		
			                }else{
		                    	return $this->redirect(['action' => 'driverlist']);			                	
			                }
		                }            		
	            	}
	                $this->Flash->error(__('Unable to update Driver Details.'));
	            }
	            $this->set('edriver', $drivers);                
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }

        public function delete($id){
        	if($this->_isAdmin() || $this->_isAgency()){
	            $this->request->allowMethod(['post', 'delete']);
	            $deletedriver = $this->Drivers->get($id);
	            $deletedriver->archived = true;
	            if ($this->Drivers->save($deletedriver)) {
	            	$userdriver = $this->Users->get($deletedriver['users_id']);
	            	$userdriver->archived = true;
	            	if($this->Users->save($userdriver)){
	                	$this->Flash->success(__('The Driver with id: {0} has been deleted.', h($id)));
	            	}else{
	            		$this->Flash->error(__('Driver Deleted , But Login not deleted', h($id)));
	            	}
	                if($this->_isAgency()){
	                	return $this->redirect(['controller' => 'Agents','action' => 'adriverlist']);		
	                }else{
	                	return $this->redirect(['action' => 'driverlist']);	                	
	                }
	            }                
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }
    } 
?>