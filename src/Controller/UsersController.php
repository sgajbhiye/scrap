<?php
    namespace App\Controller;

    use App\Controller\AppController;
    use Cake\Event\Event;
    use Cake\Utility\Security;
    use Cake\Routing\Router;
    use Cake\View\Helper\HtmlHelper;
    use Cake\Validation\Validation;
    use Cake\View\View;
    use View\Helper\FormHelper;
    use Cake\Mailer\MailerAwareTrait;
    use Cake\I18nDate;
    use Cake\Utility\Text;
    use Cake\Log\Log;
    use Cake\Mailer\Email;
    class UsersController extends AppController{
        public function initialize(){
            parent::initialize();
            $this->viewBuilder()->layout('AdminManager.dashboardlayout');
            $this->loadComponent('RequestHandler');
            $this->loadModel('Users');
            $this->loadModel('Agencys');
            $this->loadModel('Drivers');
            $this->loadModel('Scraps');
            $this->loadModel('Categorys');
            $this->loadModel('Cities');
            $this->loadModel('Trackings');
        }

        public function beforeFilter(Event $event){
            parent::beforeFilter($event);
            $this->Auth->allow(['logout','login','dforgotpassword','dchangepassword','mobilelogin','allcategory','uploadscrap','mobregister','sociallogin','viewmaterial','allcities','assignscraplist','scrapstatusupdate','trackscrap','forgotpassword','changepassword','socialuserupdatemobile','socialuserverifymobile']);
        }

        public function trackscrap(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata)) {
                    $request = json_decode($postdata);
                    $trackdata = $this->Trackings->find('all')
                                            ->where(['AND' => [
                                                    ['Trackings.archived' => 0],
                                                    ['Trackings.upload_scraps_id' => $request->scrap_id]
                                                ]
                                            ])
                                            ->order(['Trackings.id' => 'ASC']);
                        if($trackdata){
                            $result['scraptrack'] = $trackdata;
                            $result['status'] = true;
                        }else{
                            $result['status'] = false;
                        }
                }else{
                   $result['msg'] = "Not called properly for Track Map!";
                }
            $this->jsonResponse($result);
        }

        public function scrapstatusupdate(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata)) {
                $request = json_decode($postdata);
                $scrapstatus = $this->Scraps->get($request->scrapid);
                $scrapstatus->status = $request->status;
                    if($this->Scraps->save($scrapstatus)){
                        $newtrack = $this->Trackings->newEntity();
                        $track['upload_scraps_id'] = isset($request->scrapid) ? $request->scrapid : "";
                        $track['latitude'] = isset($request->lat) ? $request->lat : "";
                        $track['longitude'] = isset($request->lng) ? $request->lng : "";
                        $track['status'] = isset($request->status) ? $request->status : "";
                        $this->Trackings->patchEntity($newtrack, $track);
                        if($this->Trackings->save($newtrack)){
                            $result['scrap'] = $scrapstatus;
                            $result['status'] = true;
                        }else{
                            $result['status'] = false;
                        }
                    }else{
                        $result['status'] = false;
                    }
            }else{
               $result['msg'] = "Not called properly for change status of scrap!";
            }
            $this->jsonResponse($result);
        }

        public function assignscraplist(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata)) {
                $request = json_decode($postdata);
                $offset = isset($request->offset) ? $request->offset : 0;
                $limit = 10;
                $driverdetail = $this->getDriverDetails($request->users_id);
                if(!empty($request->status)){
                    $assignedtask = $this->Scraps->find('all')
                                            ->contain(['Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                        ]
                                                    ],'Categorys','Cities'
                                                        ,'Drivers.Users' => [
                                                            'fields' => [
                                                                'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                            ]
                                                        ]
                                                    ])
                                           ->where([ 'AND' => [
                                                            ['Scraps.archived' => 0],
                                                            ['Scraps.driver_details_id' => $driverdetail->id],
                                                            ['Scraps.status' => $request->status]
                                                        ]
                                                    ])
                                            ->order(['Scraps.id' => 'DESC'])
                                            ->offset($offset)
                                            ->limit($limit);
                }else{
                    $assignedtask = $this->Scraps->find('all')
                                            ->contain(['Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                        ]
                                                    ],'Categorys','Cities'
                                                        ,'Drivers.Users' => [
                                                            'fields' => [
                                                                'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                            ]
                                                        ]
                                                    ])
                                           ->where([ 'AND' => [
                                                            ['Scraps.archived' => 0],
                                                            ['Scraps.driver_details_id' => $driverdetail->id],
                                                            ['OR' => [
                                                                        ['Scraps.status' => 2],
                                                                        ['Scraps.status' => 3],
                                                                        ['Scraps.status' => 4],
                                                                    ]
                                                            ]
                                                        ]
                                                    ])
                                            ->order(['Scraps.id' => 'DESC']);
                }
                if($assignedtask){
                    $result['scrap'] = $assignedtask;
                    $result['status'] = true;
                }else{
                    $result['status'] = false;
                }
            }else{
               $result['msg'] = "Not called properly for assigned list of scrap!";
            }
            $this->jsonResponse($result);
        }

        public function viewmaterial(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata)) {
                    $request = json_decode($postdata);
                    $scrapdata = $this->Scraps->find('all')
                                            ->contain(['Categorys','Cities'
                                                ,'Drivers.Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ],'Agencys.Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ]
                                            ])
                                    ->where(['AND' => [
                                            ['Scraps.archived' => 0],
                                            ['Scraps.users_id' => $request->users_id]
                                        ]
                                    ])
                                    ->order(['Scraps.id' => 'DESC']);
                        if($scrapdata){
                            $result['scrap'] = $scrapdata;
                            $result['status'] = true;
                        }else{
                            $result['status'] = false;
                        }
                }else{
                   $result['msg'] = "Not called properly for material category!";
                }
            $this->jsonResponse($result);
        }

        public function allcategory(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata)) {
                    $request = json_decode($postdata);
                    $catgy = $this->Categorys->find('all')
                                    ->select(['id','name'])
                                    ->where(['Categorys.archived' => 0])
                                    ->order(['Categorys.name' => 'ASC']);
                        if($catgy){
                            $result['categories'] = $catgy;
                            $result['status'] = true;
                        }else{
                            $result['status'] = false;
                        }
                }else{
                   $result['msg'] = "Not called properly for material category!";
                }
            $this->jsonResponse($result);
        }

        public function allcities(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata)) {
                    $cities = $this->Cities->find('all')
                                    ->select(['id','name','state'])
                                    ->where(['Cities.archived' => 0])
                                    ->order(['Cities.name' => 'ASC']);
                        if($cities){
                            $result['cities'] = $cities;
                            $result['status'] = true;
                        }else{
                            $result['status'] = false;
                        }
                }else{
                   $result['msg'] = "Not called properly for material cities!";
                }
            $this->jsonResponse($result);
        }

        public function uploadscrap(){
            if (isset($this->request->data['sdata']) && !empty($this->request->data['sdata'])){
                $request = json_decode($this->request->data['sdata']);
                $scrap = $this->Scraps->newEntity();
                $scrapData['users_id'] = $request->users_id;
                $scrapData['description'] = isset($request->description) ? $request->description : "";
                $scrapData['address'] = isset($request->address) ? $request->address : "";
                $scrapData['city_id'] = isset($request->city) ? $request->city : "";
                $scrapData['scrap_category_id'] = $request->category;
                $scrapData['scrapimg'] = $this->uploadScrapImg();
                $scrapData['lat'] = isset($request->lat) ? $request->lat : "";
                $scrapData['longi'] = isset($request->longi) ? $request->longi : "";
                $this->Scraps->patchEntity($scrap, $scrapData);
                if($this->Scraps->save($scrap)){
                    $result['scrapid'] = $scrap->id;
                    $result['status'] = true;
                }else{
                    $result['status'] = false;
                }
            }else{
                $result['msg'] = "Not called properly for upload scrap!";
            }
            $this->jsonResponse($result);
        }
        // Log::write('debug', json_encode($result, JSON_PRETTY_PRINT));

        protected function uploadScrapImg(){
            if (isset($this->request->data['file']) && !empty($this->request->data['file'])){
                $target_path = WWW_ROOT."img/uploads/";
                $time = idate("i").idate("s");
                $target_pathnew = $target_path . $time . basename( $_FILES['file']['name']);
                if (move_uploaded_file($_FILES['file']['tmp_name'], $target_pathnew)) {
                    return "img/uploads/". $time . $_FILES['file']['name'];
                }
            }
            return false;
        }

        //     Log::write('debug', json_encode($_POST, JSON_PRETTY_PRINT));
        //     Log::write('debug', json_encode($_FILES, JSON_PRETTY_PRINT));
        //     Log::write('debug', json_encode($this->request->data, JSON_PRETTY_PRINT));
        //     Log::write('debug', json_encode($this->request, JSON_PRETTY_PRINT));
        //     debug($_POST);
        //     debug($_FILES);
        //     debug($this->request->data);
        //     exit();

        public function sociallogin(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata) && !empty($postdata)) {
                $request = json_decode($postdata);
                $exists = $this->Users->exists(['Users.email' => trim($request->email," ")]);
                if(!$exists){
                    $user = $this->Users->newEntity();
                    $this->request->data['fname'] = isset($request->name) ? $request->name : null;
                    $this->request->data['mobile'] = isset($request->mobile) ? $request->mobile : null;
                    $this->request->data['email'] = $request->email;
                    $this->request->data['provider'] = isset($request->provider) ? $request->provider : null;
                    $this->request->data['provider_id'] = isset($request->provider_id) ? $request->provider_id : null;
                    $this->request->data['social_status'] = false;
                    $user = $this->Users->patchEntity($user, $this->request->getdata());
                    if($this->Users->save($user)){
                        $result['userid'] = $user->id;
                        $result['social_status'] = $user->social_status;
                        $result['status'] = true;
                    }else{
                        $result['status'] = false;
                    }
                }else{
                    $olduser = $this->Users->find()
                                        ->where(['Users.email' => trim($request->email," ")])
                                        ->first();
                    $result['status'] = true;
                    $result['old'] = true;
                    $result['userid'] = $olduser->id;
                    $result['social_status'] = $olduser->social_status;
                    $result['msg'] = "Old User, Email Already Registered";
                }
            }else{
                $result['msg'] = "Not called properly for sociallogin!";
            }
            $this->jsonResponse($result);
        }

        public function socialuserupdatemobile(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata) && !empty($postdata)) {
                $otp = mt_rand(100000, 999999);
                $request = json_decode($postdata);
                $userId = isset($request->users_id) ? $request->users_id : null;
                $mobile = isset($request->mobile) ? $request->mobile : null;
                if(!empty($userId && $mobile)){
                    $socialuser = $this->Users->get($userId);
                    $socialuser->otp = $otp;
                    $socialuser->social_status = false;
                    $socialuser->mobile = $mobile;
                    if ($this->Users->save($socialuser)) {
                        if($this->sendOtpViaSms($mobile,$otp)){
                            $result['status'] = true;
                            $result['userid'] = $userId;
                        }else{
                            $result['msg'] = "Unable To send OTP via SMS, Please Try Again..!";
                        }
                    }
                }else{
                    $result['msg'] = "UserId Or Mobile is Not present";
                }
            }else{
                $result['msg'] = "Not called properly for sociallogin!";
            }
            $this->jsonResponse($result);
        }

        public function socialuserverifymobile(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata) && !empty($postdata)) {
                $request = json_decode($postdata);
                $userId = $request->UserId;
                $otp = $request->Otp;
                if(!empty($userId)){
                    $verifyuser = $this->Users->get($userId);
                    if($verifyuser->otp == $otp){
                        $verifyuser->social_status = null;
                        $verifyuser->otp = null;
                        if($this->Users->save($verifyuser)){
                            $result['msg'] = 'User Successfuly Verified';
                            $result['userid'] = $verifyuser->id;
                            $result['social_status'] = $verifyuser->social_status;
                            $result['status'] = true;
                        }
                    }else{
                        $result['msg'] = "OTP Not Matched..! Try Again";
                    }
                }else{
                    $result['msg'] = "Error...! User Id Not present";
                }
            }else{
                $result['msg'] = "Not called properly for changepassword!";
            }
            $this->jsonResponse($result);
        }

        public function mobregister(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata) && !empty($postdata)) {
                $request = json_decode($postdata);
                // $request->password = trim($request->password,"");
                // $request->confirmpassword = trim($request->confirmpassword,"");
                // if((isset($request['password']) && isset($request['confirmpassword '])) != '' && ($request['password'] == $request['confirmpassword '])){

                // }else{
                //   $result['msg'] = "Password Not Match!";
                // }
                $exists = $this->Users->exists(['Users.email' => trim($request->email," ")]);
                if(!$exists){
                    $user = $this->Users->newEntity();
                    $this->request->data['fname'] = isset($request->firstname) ? $request->firstname : null;
                    $this->request->data['lname'] = isset($request->lastname) ? $request->lastname : null;
                    $this->request->data['gender'] = isset($request->gender) ? $request->gender : null;
                    $this->request->data['mobile'] = isset($request->mobilenumber) ? $request->mobilenumber : null;
                    $this->request->data['email'] = $request->email;
                    $this->request->data['password'] = $request->password;
                    $this->request->data['address'] = isset($request->address) ? $request->address : null;
                    $this->request->data['city_id'] = isset($request->city) ? $request->city : null;
                    $this->request->data['state'] = isset($request->state) ? $request->state : null;
                    $this->request->data['postalcode'] = isset($request->pin) ? $request->pin : null;
                    $user = $this->Users->patchEntity($user, $this->request->getdata());
                    if($this->Users->save($user)){
                        $result['userid'] = $user->id;
                        $result['status'] = true;
                    }else{
                        $result['status'] = false;
                    }
                }else{
                    $result['status'] = false;
                    $result['msg'] = "Email Already Registered";
                }
            }else{
                $result['msg'] = "Not called properly for register user!";
            }
            $this->jsonResponse($result);
        }

        public function mobilelogin(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
                if (isset($postdata) && !empty($postdata)) {
                    $request = json_decode($postdata);
                    $username = $request->username;
                    $password = $request->password;

                        $this->Auth->config('authenticate', [
                            'Form' => [
                                'fields' => ['username' => 'email', 'password' => 'password'],
                                'scope' => ['archived' => 0]
                                ],
                        ]);
                        $this->Auth->constructAuthenticate();
                        $this->request->data['email'] = $username;
                        $this->request->data['password'] = $password;
                        $user = $this->Auth->identify();
                        if($user){
                            $result['user'] = $user;
                            $result['status'] = true;
                        }else{
                            $result['status'] = false;
                        }
                }else{
                    $result['msg'] = "Not called properly with username parameter!";
                }
            $this->jsonResponse($result);
        }

        public function forgotpassword(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata) && !empty($postdata)) {
                $request = json_decode($postdata);
                $email = $request->email;
                $finduser = $this->Users->find()
                                    ->where(['Users.email' => trim($email," "), 'Users.archived' => 0])->first();
                if(!empty($finduser)){
                    $userId = $finduser->id;
                    $otp = mt_rand(100000, 999999);
                    $userotpupdate = $this->Users->get($userId);
                    $userotpupdate->otp = $otp;
                    if ($this->Users->save($userotpupdate)) {
                        if($this->sendemailotp($email,$otp)){
                            $result['status'] = true;
                            $result['userid'] = $userId;
                        }else{
                            $result['msg'] = "Unable To send OTP on Email, Please Try Again..!";
                        }
                    }
                }else{
                    $result['msg'] = "Email Not Found Or Account Suspended..!";
                }
            }else{
                $result['msg'] = "Not called properly for forgotpassword";
            }
            $this->jsonResponse($result);
        }

        public function changepassword(){
            $result['status'] = false;
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 86400');    // cache for 1 day
            }
            // Access-Control headers are received during OPTIONS requests
            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
                exit(0);
            }
            $postdata = file_get_contents("php://input");
            if (isset($postdata) && !empty($postdata)) {
                $request = json_decode($postdata);
                $userId = $request->UserId;
                $password = $request->Password;
                $cpassword = $request->ConfirmPassword;
                $otp = $request->Otp;
                if(!empty($userId)){
                    if(!empty($password)){
                        if($password == $cpassword){
                            $changeuser = $this->Users->get($userId);
                            if($changeuser->otp == $otp){
                                $changeuser->password = $password;
                                $changeuser->otp = null;
                                if($this->Users->save($changeuser)){
                                    $result['msg'] = 'Password Changed Successfully. Please Login With New Password';
                                    $result['status'] = true;
                                }
                            }else{
                                $result['msg'] = "OTP Not Matched..! Try Again";
                            }
                        }else{
                            $result['msg'] = "Password & Confirm Password Not Matched..!";
                        }
                    }else{
                        $result['msg'] = "Error...! Password Not present";
                    }
                }else{
                    $result['msg'] = "Error...! User Id Not present";
                }
            }else{
                $result['msg'] = "Not called properly for changepassword!";
            }
            $this->jsonResponse($result);
        }

        public function alluser(){
            if($this->_isAdmin()){
                $users =  $this->Users->find('all')
                                        ->select(['id','fname','lname','mobile','email','address','gender','dob'])
                                        ->where(['Users.archived' => 0, 'Users.role_id' => 2])
                                        ->order(['Users.fname' => 'ASC']);
                                        // ->where(['Users.archived' => 0, 'Users.role_id <' => 3 , 'Users.role_id !=' => 4])
                $this->set('userslist',$users);
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function edituser($id = null){
            if($this->_isAdmin()){
                $edituser = $this->Users->get($id);
                if ($this->request->is(['post', 'put'])) {
                    $this->request->data['dob'] = date('Y-m-d',strtotime($this->request->data['dob']));
                    $this->Users->patchEntity($edituser, $this->request->getData());
                    if ($this->Users->save($edituser)) {
                        $this->Flash->success(__('User has been updated.'));
                        return $this->redirect(['action' => 'alluser']);
                    }
                    $this->Flash->error(__('Unable to update user.'));
                }
                $this->set('edituser', $edituser);
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function deleteuser($id){
            if($this->_isAdmin()){
                $this->request->allowMethod(['post', 'deleteuser']);
                $edituser = $this->Users->get($id);
                $edituser->archived = true;
                if ($this->Users->save($edituser)) {
                    $this->Flash->success(__('The user with id: {0} has been deleted.', h($id)));
                    return $this->redirect(['action' => 'alluser']);
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function alluploads(){
            if($this->_isAdmin()){
                $alluploads = $this->Scraps->find('all')
                                    ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],'Categorys','Cities'])
                                    ->where(['Scraps.archived' => 0,
                                            'Scraps.status' => 0
                                    ])
                                    ->order(['Scraps.id' => 'DESC']);
                $this->set(compact('alluploads'));
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function allpending(){
            if($this->_isAdmin()){
                $allpendingtask = $this->Scraps->find('all')
                                ->contain(['Users' => [
                                            'fields' => [
                                                'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                            ]
                                        ],'Categorys','Cities'
                                            ,'Agencys.Users' => [
                                                'fields' => [
                                                    'Users.id','Users.mobile','Users.email','Users.address'
                                                ]
                                            ]
                                        ])
                                ->where(['Scraps.archived' => 0,
                                        'Scraps.status' => 1
                                ])
                                ->order(['Scraps.id' => 'DESC']);
                $this->set(compact('allpendingtask'));
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function allassigned(){
            if($this->_isAdmin()){
                $allassignedtask = $this->Scraps->find('all')
                                ->contain(['Users' => [
                                            'fields' => [
                                                'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                            ]
                                        ],'Categorys','Cities',
                                            'Agencys.Users' => [
                                                'fields' => [
                                                    'Users.id','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],
                                            'Drivers.Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ]
                                        ])
                                ->where(['Scraps.archived' => 0,
                                        'Scraps.status' => 2
                                ])
                                ->order(['Scraps.id' => 'DESC']);
                $this->set(compact('allassignedtask'));
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function allinprocess(){
            if($this->_isAdmin()){
                $allinprocesstask = $this->Scraps->find('all')
                                ->contain(['Users' => [
                                            'fields' => [
                                                'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                            ]
                                        ],'Categorys','Cities',
                                            'Agencys.Users' => [
                                                'fields' => [
                                                    'Users.id','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],
                                            'Drivers.Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ]
                                        ])
                                ->where(['Scraps.archived' => 0,
                                       'OR' => [
                                           ['Scraps.status' => 3],
                                           ['Scraps.status' => 4]
                                       ]
                                ])
                                ->order(['Scraps.id' => 'DESC']);
                $this->set(compact('allinprocesstask'));
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function allcomplete(){
            if($this->_isAdmin()){
                if($this->request->is('post')){
                    $startdate =  date("Y-m-d", strtotime($this->request->data['startDate']));
                    $enddate =  date("Y-m-d", strtotime($this->request->data['endDate']));
                    $allcompletetask = $this->Scraps->find('all')
                                        ->contain(['Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ],'Categorys','Cities',
                                                    'Agencys.Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.mobile','Users.email','Users.address'
                                                        ]
                                                    ],
                                                    'Drivers.Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                        ]
                                                    ]
                                                ])
                                        ->where(['Scraps.archived' => 0,
                                                'Scraps.status' => 5,
                                                'Scraps.modified BETWEEN :start AND :end'
                                        ])
                                        ->order(['Scraps.id' => 'DESC'])
                                        ->bind(':start', new \DateTime($startdate), 'date')
                                        ->bind(':end',   new \DateTime($enddate), 'date');
                    $this->set(compact('startdate','enddate'));
                }else{
                    $allcompletetask = $this->Scraps->find('all')
                                    ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],'Categorys','Cities',
                                                'Agencys.Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ],
                                                'Drivers.Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ]
                                            ])
                                    ->where(['Scraps.archived' => 0,
                                            'Scraps.status' => 5
                                    ])
                                    ->order(['Scraps.id' => 'DESC']);
                }
                $this->set(compact('allcompletetask'));
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function viewmap($id = null){
            if(!empty($id)){
                $scrap = $this->Scraps->get($id);
                $scrapcomplete = $this->Trackings->find()
                                                ->contain([
                                                    'Scraps.Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile'
                                                        ]
                                                    ],
                                                    'Scraps.Drivers.Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile'
                                                        ]
                                                    ],
                                                    'Scraps.Agencys'
                                                ])
                                                ->where([
                                                    'Trackings.upload_scraps_id' => $id,
                                                    'Trackings.status' => 5
                                                ])
                                                ->last();
                // pr($scrapcomplete->toarray()); exit;
                $this->set(compact('scrap','scrapcomplete'));
            }
        }

        public function deletecategory($id){
            if($this->_isAdmin()){
                $this->request->allowMethod(['post', 'deleteuser']);
                $deletecat = $this->Categorys->get($id);
                $deletecat->archived = true;
                if ($this->Categorys->save($deletecat)) {
                    $this->Flash->success(__('The category with id: {0} has been deleted.', h($id)));
                    return $this->redirect(['action' => 'category']);
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function dashboard(){
            $usercount = $this->Users->find()
                                ->where(['AND' => [
                                        ['Users.archived' => 0],
                                        ['OR' => [
                                                    ['Users.role_id' => 2],
                                                    ['Users.role_id' => 1]
                                                ]
                                        ]
                                    ]])
                                ->count();
            $agencycount = $this->Agencys->find()
                                ->where(['Agencys.archived' => 0])
                                ->count();
            $drivercount = $this->Drivers->find()
                                ->where(['Drivers.archived' => 0])
                                ->count();
            $this->set('ucount', $usercount);
            $this->set('acount', $agencycount);
            $this->set('dcount', $drivercount);
            if($this->_isAgency()){
                $currentagencydetails = $this->getAgencyCategory($this->Auth->user('id'));
                $categoryarr = explode(',', $currentagencydetails->category);
                $agencynewtask = $this->Scraps->find()
                                            ->where(['AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.status' => 0],
                                                    ['Scraps.city_id' => $currentagencydetails['user']->city_id],
                                                    ['Scraps.scrap_category_id IN' => $categoryarr]
                                                ]])
                                            ->count();
                $agencyaccepttask = $this->Scraps->find()
                                            ->where(['AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.status' => 1],
                                                    ['Scraps.agency_details_id' => $currentagencydetails->id]
                                                ]])
                                            ->count();
                $agencydonetask = $this->Scraps->find()
                                            ->where(['AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.status' => 5],
                                                    ['Scraps.agency_details_id' => $currentagencydetails->id]
                                                ]])
                                            ->count();
                $agencydrivercount = $this->Drivers->find()
                                ->where(['AND' => [
                                        ['Drivers.archived' => 0],
                                        ['Drivers.agency_details_id' => $currentagencydetails->id],
                                    ]])
                                ->count();
                $this->set('anewtaskcount', $agencynewtask);
                $this->set('aaccepttaskcount', $agencyaccepttask);
                $this->set('adonetaskcount', $agencydonetask);
                $this->set('adrivercount', $agencydrivercount);
            }
        }

        public function user(){
            if($this->_isAdmin()){
                if($this->request->is('post')){
                    $exists = $this->Users->exists(['Users.email' => trim($this->request->data['email']," ")]);
                    if(!$exists){
                        $user = $this->Users->newEntity();
                        $this->request->data['dob'] = date('Y-m-d',strtotime($this->request->data['dob']));
                        $user = $this->Users->patchEntity($user, $this->request->getdata());
                        if($this->Users->save($user)){
                            $this->Flash->success(__('User Details Saved'));
                            return $this->redirect(['action' => 'alluser']);
                        }else{
                            $this->Flash->error(__('Users Details Not Saved. Try Again'));
                        }
                    }else{
                        $this->Flash->error(__('Email Already Registered'));
                    }
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function drivers(){
            if($this->_isAdmin() || $this->_isAgency()){
                if($this->request->is('post')){
                    $exists = $this->Users->exists(['Users.email' => trim($this->request->data['email']," ")]);
                    if(!$exists){
                        $user = $this->Users->newEntity();
                        $this->request->data['dob'] = date('Y-m-d',strtotime($this->request->data['dob']));
                        $user = $this->Users->patchEntity($user, $this->request->getdata());
                        if($this->Users->save($user)){
                            $this->request->data['users_id'] = $user->id;
                            $this->request->data['drivinglicense'] = $this->imageupload($this->request->data['lisenceFile'], 'DrivingLisence');
                            $driver = $this->Drivers->newEntity();
                            $driver = $this->Drivers->patchEntity($driver, $this->request->getdata());
                            if($this->Drivers->save($driver)){
                                $this->Flash->success(__('Details Saved Successfully'));
                                if($this->_isAgency()){
                                    return $this->redirect(['controller' => 'Agents','action' => 'adriverlist']);
                                }else{
                                    return $this->redirect(['controller' => 'Drivers','action' => 'driverlist']);
                                }
                            }else{
                                $this->Flash->error(__('Only Users Details Saved, Please contact admin to save driver details'));
                            }
                        }else{
                            $this->Flash->error(__('Users Details Not Saved. Try Again'));
                        }
                    }else{
                        $this->Flash->error(__('Email Already Registered'));
                    }
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function agency(){
            if($this->_isAdmin()){
                if($this->request->is('post')){
                    $exists = $this->Users->exists(['Users.email' => trim($this->request->data['email']," ")]);
                    if(!$exists){
                        $user = $this->Users->newEntity();
                        $this->request->data['category'] = implode(",",$this->request->data['category']);
                        $user = $this->Users->patchEntity($user, $this->request->getdata());
                        if($this->Users->save($user)){
                            $this->request->data['users_id'] = $user->id;
                            $agency = $this->Agencys->newEntity();
                            $agency = $this->Agencys->patchEntity($agency, $this->request->getdata());
                            if($this->Agencys->save($agency)){
                                $this->Flash->success(__('User Details Saved'));
                                return $this->redirect(['controller' => 'Agents','action' => 'agencylist']);
                            }else{
                                $this->Flash->error(__('Only Users Details Saved, Please contact admin to save agency details'));
                            }
                        }else{
                            $this->Flash->error(__('Users Details Not Saved. Try Again'));
                        }
                    }else{
                        $this->Flash->error(__('Email Already Registered'));
                    }
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function category(){
            if($this->_isAdmin()){
                if($this->request->is('post')){
                    $category = $this->Categorys->newEntity();
                    $category = $this->Categorys->patchEntity($category, $this->request->getdata());
                    if($this->Categorys->save($category)){
                        $this->Flash->success(__('Category Details Saved'));
                        return $this->redirect(['action' => 'category']);
                    }else{
                        $this->Flash->error(__('Category Details Not Saved'));
                    }
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function editcategory($id = null){
            if($this->_isAdmin()){
                $editcat = $this->Categorys->get($id);
                if($this->request->is(['post','put'])){
                    $this->Categorys->patchEntity($editcat, $this->request->getdata());
                    if($this->Categorys->save($editcat)){
                        $this->Flash->success(__('Category Details Updated'));
                        return $this->redirect(['action' => 'category']);
                    }else{
                        $this->Flash->error(__('Failed to Updated Category Detials'));
                    }
                }
                $this->set('editcat', $editcat);
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function citylist(){
            if($this->_isAdmin()){
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function addcity(){
            if($this->_isAdmin()){
                if($this->request->is('post')){
                    $newcity = $this->Cities->newEntity();
                    $this->Cities->patchEntity($newcity, $this->request->getData());
                    if($this->Cities->save($newcity)){
                        $this->Flash->success(__('City Details Saved'));
                        return $this->redirect(['action' => 'citylist']);
                    }else{
                        $this->Flash->error(__('Failed to Add City Details'));
                    }
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function editcity($id = null){
            if($this->_isAdmin()){
                $editcity = $this->Cities->get($id);
                if($this->request->is(['post','put'])){
                    $this->Cities->patchEntity($editcity, $this->request->getdata());
                    if($this->Cities->save($editcity)){
                        $this->Flash->success(__('City Details Updated'));
                        return $this->redirect(['action' => 'citylist']);
                    }else{
                        $this->Flash->error(__('Failed to Updated City Detials'));
                    }
                }
                $this->set('editcity', $editcity);
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function deletecity($id){
            if($this->_isAdmin()){
                $this->request->allowMethod(['post', 'deletecity']);
                $deletecity = $this->Cities->get($id);
                $deletecity->archived = true;
                if ($this->Cities->save($deletecity)) {
                    $this->Flash->success(__('The city with id: {0} has been deleted.', h($id)));
                    return $this->redirect(['action' => 'citylist']);
                }
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout());
            }
        }

        public function login(){
            // pr($this->request->data); exit;
            $this->viewBuilder()->layout('AdminManager.login');
            if ($this->request->is('post')) {
                $this->Auth->config('authenticate', [
                    'Form' => [
                        'fields' => ['username' => 'email'],
                        'scope' => ['archived' => 0]
                        ],
                ]);
                $this->Auth->constructAuthenticate();
                $this->request->data['email'] = $this->request->data['username'];
                $user = $this->Auth->identify();
                if($user){
                    $this->Auth->setUser($user);
                    if($this->_isAdmin() || $this->_isUser() || $this->_isAgency() || $this->_isDriver()){
                        $this->Auth->setUser($user);
                        return $this->redirect($this->Auth->redirectUrl());
                    }else{
                        $this->Flash->error(__('Technical Error !! Try Again Later'));
                        return $this->redirect($this->Auth->logout());
                    }
                }
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }

        public function dforgotpassword(){
            $this->viewBuilder()->layout('AdminManager.login');
            if($this->request->is('post')){
                $email = $this->request->data['email'];
                $finduser = $this->Users->find()
                                    ->where(['Users.email' => trim($email," "), 'Users.archived' => 0])->first();
                if(!empty($finduser)){
                    $userId = $finduser->id;
                    $otp = mt_rand(100000, 999999);
                    $userotpupdate = $this->Users->get($userId);
                    $userotpupdate->otp = $otp;
                    if ($this->Users->save($userotpupdate)) {
                        if($this->sendemailotp($email,$otp)){
                            $this->redirect(['action' => 'dchangepassword', $userId]);
                        }else{
                            $this->Flash->error(__('Unable To Send OTP on email.. Try Again.!!'));
                        }
                    }
                }else{
                    $this->Flash->error(__('Email Not Found Or Account Suspended.!!'));
                }
            }
        }

        public function dchangepassword($userId = null){
            $this->viewBuilder()->layout('AdminManager.login');
            if(!empty($userId)){
                if($this->request->is('post')){
                    $password = $this->request->data['password'];
                    $cpassword = $this->request->data['cpassword'];
                    $otp = $this->request->data['otp'];
                    if(!empty($password)){
                        if($password == $cpassword){
                            $changeuser = $this->Users->get($userId);
                            if($changeuser->otp == $otp){
                                $changeuser->password = $password;
                                $changeuser->otp = null;
                                if($this->Users->save($changeuser)){
                                    $this->redirect(['action' => 'login']);
                                    $this->Flash->success(__('Password Changed Successfully. Please Login With New Password'));
                                }
                            }else{
                                $this->Flash->error(__('OTP Not Matched..! Try Again..!!'));
                            }
                        }else{
                            $this->Flash->error(__('Password & Confirm Password Not Matched..!'));
                        }
                    }else{
                        $this->Flash->error(__('Error...! Password Not present'));
                    }
                }
            }else{
                return $this->redirect(['action' => 'login']);
            }
        }

        public function logout(){
            return $this->redirect($this->Auth->logout());
        }

        public function test(){
            // $current_year = date('y');
            // $next_year = $current_year + 1;
            // $id = 1;
            // print_r('WS'.$current_year .'-'. $next_year .'-A'. sprintf("%'.04d\n", $id));
            exit;
        }
    }
?>