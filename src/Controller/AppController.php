<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller{
    public function beforeFilter(Event $event){
        if($this->_isAdmin() || $this->_isAgency() || $this->_isUser()){
            $cities = $this->Cities->find('all')
                                    ->where(['Cities.archived' => 0])
                                    ->order(['Cities.name' => 'ASC']);
            $this->set('cities',$cities);
        }
        $catgy = $this->Categorys->find('all')
                                    ->where(['Categorys.archived' => 0])
                                    ->order(['Categorys.name' => 'ASC']);
        $this->set('catgy',$catgy);
        $agency =  $this->Agencys->find('all')
                                    ->select(['id','agencyname'])
                                    ->where(['Agencys.archived' => 0])
                                    ->order(['Agencys.agencyname' => 'ASC']);
        $this->set('agency',$agency);
        if($this->_isAgency()){
            $singleagencydetail = $this->Agencys->find()
                                ->where(['AND' => [
                                            ['Agencys.users_id' => $this->Auth->user('id')],
                                            ['Agencys.archived' => 0]
                                    ]])
                                ->first();
            $this->set('singleagency', $singleagencydetail);
        }
        if($this->_isAdmin() || $this->_isUser() || $this->_isDriver()){
            $this->set('nameofloggeduser', $this->Auth->user('fname')." ".$this->Auth->user('lname'));
        }
        $this->set('admin', $this->_isAdmin());
        $this->set('loggeduser', $this->_isUser());
        $this->set('loggedagency', $this->_isAgency());
        $this->set('loggeddriver', $this->_isDriver());
    }

    public function initialize(){
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
        'loginRedirect' => [
            'controller' => 'Users',
            'action'     => 'dashboard'
        ],
        'logoutRedirect' => [
            'controller' => 'Users',
            'action'     => 'login'
        ]
        ]);
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']
                ]
            ]
        ]);
    }

    public function beforeRender(Event $event){
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    protected function _isAdmin(){
        if($this->Auth->user('role_id') != 1){
            return false;
        }
        return true;
    }

    protected function _isUser(){
        if($this->Auth->user('role_id') != 2){
            return false;
        }
        return true;
    }

    protected function _isAgency(){
        if($this->Auth->user('role_id') != 3){
            return false;
        }
        return true;
    }

    protected function _isDriver(){
        if($this->Auth->user('role_id') != 4){
            return false;
        }
        return true;
    }

    public function jsonResponse($response = ''){
        $this->autoRender = false ;
        $this->response->type('json');
        $response = json_encode($response);
        $this->response->body($response);
    }

    protected function getAgencyCategory($id = null){
        if(isset($id) && $id != null){
            $agencydetail = $this->Agencys->find()
                                ->contain(['Users' => [
                                    'fields' => ['Users.city_id']
                                    ]])
                                ->where(['AND' => [
                                            ['Agencys.users_id' => $id],
                                            ['Agencys.archived' => 0]
                                    ]])
                                ->first();
            return $agencydetail;
        }else{
            return false;
        }
    }

    protected function getDriverDetails($id = null){
        if(isset($id) && $id != null){
            $driverdetail = $this->Drivers->find()
                                ->where(['AND' => [
                                            ['Drivers.users_id' => $id],
                                            ['Drivers.archived' => 0]
                                    ]])
                                ->first();
            return $driverdetail;
        }else{
            return false;
        }
    }

    protected function imageupload($imgdata = Array(), $type){
        if(!empty($imgdata)){
            $img = $imgdata;
            $randomName = rand(10,1000000);
            if (!file_exists(WWW_ROOT . 'img/'.$type.'/' . date("Y"))){
                mkdir(WWW_ROOT . 'img/'.$type.'/' . date("Y"), 0777, true);
            }
            $target_file = WWW_ROOT . 'img/'.$type.'/' . date("Y") . '/' . basename($img['name']);
            $imgSize = $img['size'];
            if($imgSize > 8000000){
                return false;
            }else{
                $imgFleTyp = pathinfo($target_file,PATHINFO_EXTENSION);
                $imgFleTyp = strtolower($imgFleTyp);
                if($imgFleTyp != "jpg" && $imgFleTyp != "png" && $imgFleTyp != "jpeg" && $imgFleTyp != "gif" ){
                    return false;
                }else{
                    $target_file_withNewName = WWW_ROOT . 'img/'.$type.'/'. date("Y") . '/' . $randomName . $img['name'];
                    if(move_uploaded_file($img['tmp_name'], $target_file_withNewName)){
                       return 'img/'.$type.'/'. date("Y") . '/' . $randomName . $img['name'];
                    }else{
                        return false;
                    }
                }
            }
        }
        return false;
    }

    protected function sendemailotp($email,$otp){
        $url = 'https://api.sendgrid.com/';
        $user = 'soilon';
        $pass = 'number1email';
        $json_string = array(
            'category' => 'Forgot Password OTP ( Team SoilOn )'
        );
        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'x-smtpapi' => json_encode($json_string),
            'to'        => $email,
            'subject'   => 'OTP For Forgot Password ( Team SoilOn )',
            'html'      => 'Team SoilOn is thanking you for submitting request for change password!!<br><br>Your OTP For changing password is : <b>' . $otp . '</b>',
            'text'      => 'Team SoilOn is thanking you for submitting request for change password!!<br><br>Your OTP For changing password is : <b>' . $otp . '</b>',
            'from'      => 'soilonmarketing@gmail.com',
        );
        $request =  $url.'api/mail.send.json';
        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // obtain response
        $response = curl_exec($session);
        curl_close($session);
        // print everything out
        $responseconverted = json_decode($response, true);
        if(isset($responseconverted['errors'])){
            return false;
        }else{
            return true;
        }
    }

    protected function sendOtpViaSms($mob,$otp){
        $mobile = urlencode($mob);
        $msg = "Your SOILON OTP is ". $otp;
        $username = urlencode('SoilonLL');
        $key = urlencode('8aa646b5a2XX');
        $message = urlencode($msg);
        $senderid = urlencode('INFOSM');
        $accusage = urlencode('1');
        $data = 'user='.$username.'&key='.$key.'&mobile='.$mobile.'&message='.$message.'&senderid='.$senderid.'&accusage='.$accusage;
        $ch = curl_init('http://103.233.79.246//submitsms.jsp?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $submit = curl_exec($ch);
        $ch;
        curl_close($ch);
        $arr = explode(",",$submit);
        if(!empty($arr[1]) && $arr[1] == 'success'){
            return true;
        }else{
            return false;
        }
        return false;
    }
}
