<?php
    namespace App\Controller;

    use App\Controller\AppController;
    use Cake\Event\Event;
    use Cake\Utility\Security;
    use Cake\Routing\Router;
    use Cake\View\Helper\HtmlHelper;
    use Cake\Validation\Validation;
    use Cake\View\View;
    use View\Helper\FormHelper;
    use Cake\Mailer\MailerAwareTrait;
    use Cake\I18nDate;
    use Cake\I18n\Time;
    use Cake\Utility\Text;  
    class AgentsController extends AppController{
        public function initialize(){
            parent::initialize();
            $this->viewBuilder()->layout('AdminManager.dashboardlayout');
            $this->loadComponent('RequestHandler');
            $this->loadModel('Users');
            $this->loadModel('Agencys');
            $this->loadModel('Drivers');
            $this->loadModel('Categorys');
            $this->loadModel('Scraps');
            $this->loadModel('Cities');
            $this->loadModel('Trackings');
        }

        public function beforeFilter(Event $event){
            parent::beforeFilter($event);
        }

        public function agencylist(){
        	if($this->_isAdmin()){
	            // $agencies = $this->Agencys->find('all')
             //                            ->contain(['Users' => [ 
             //                                        'fields' => [
             //                                                        'Users.id','Users.mobile','Users.address','Users.city_id','Users.state'
             //                                                    ]
             //                                        ],'Users.Cities'])
	            //                         ->where(['Agencys.archived' => 0])
	            //                         ->order(['Agencys.agencyname' => 'ASC']);
                if(isset($_GET['slimit']) && !empty($_GET['slimit'])){
                    $setlimit = $_GET['slimit'];
                }else{
                    $setlimit = 10; 
                }
                $option = [
                    'conditions' => [
                        'Agencys.archived' => 0 
                    ],
                    'contain' => [
                        'Users' => [ 
                            'fields' => [
                                            'Users.id','Users.mobile','Users.address','Users.city_id','Users.state'
                                        ]
                            ],
                        'Users.Cities'   
                    ],
                    'order' => [
                        'Agencys.agencyname' => 'ASC'  
                    ],
                    'limit' => $setlimit
                ];
                $this->paginate = $option;
                $agencies = $this->paginate($this->Agencys);
                $this->set('slimit',$setlimit);   
                $this->set('agencies',$agencies);
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }

        public function edit($id = null){
        	if($this->_isAdmin()){
	            $edit = $this->Agencys->find()
	            						->contain(['Users' => [
		            								'fields' => [
		            												'Users.id','Users.mobile','Users.address','Users.city_id','Users.state'
		            											]
		            								]
		            							])
	            						->where(['Agencys.id' => $id])
	            						->first();
	            if ($this->request->is(['post', 'put'])) {
	            	$user = $this->Users->get($edit['users_id']);
	            	$user = $this->Users->patchEntity($user, $this->request->getdata());
	            	if($this->Users->save($user)){
	            		$agency = $this->Agencys->get($id);
	            		$this->request->data['category'] = implode(",",$this->request->data['category']);
		                $this->Agencys->patchEntity($agency, $this->request->getData());
		                if ($this->Agencys->save($agency)) {
		                    $this->Flash->success(__('Agency Details has been updated.'));
		                    return $this->redirect(['action' => 'agencylist']);
		                }            		
	            	}
	                $this->Flash->error(__('Unable to update Agency Details.'));
	            }
	            $this->set('edit', $edit);                
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }

        public function delete($id){
        	if($this->_isAdmin()){
	            $this->request->allowMethod(['post', 'delete']);
	            $deleteagency = $this->Agencys->get($id);
	            $deleteagency->archived = true;
	            if ($this->Agencys->save($deleteagency)) {
	            	$userdel = $this->Users->get($deleteagency['users_id']);
	            	$userdel->archived = true;
	            	if($this->Users->save($userdel)){
	                	$this->Flash->success(__('The Agency with id: {0} has been deleted.', h($id)));
	            	}else{
	            		$this->Flash->error(__('Agency Deleted, Error in Deleting Agency Login Details.', h($id)));
	            	}
	                return $this->redirect(['action' => 'agencylist']);
	            }                
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }

        public function adriverlist(){
        	if($this->_isAgency()){
	        	$drivers = $this->Drivers->find('all')
	            						->contain(['Users' => [
	            								'fields' => [
	            									'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
	            								]
	            							],'Agencys'])
	                                    ->where(['AND' => [
	                                    				['Drivers.archived' => 0],
	                                    				['Agencys.users_id' => $this->Auth->user('id')]
	                                    			]
	                                    		])
	                                    ->order(['Users.fname' => 'ASC']);
	            $this->set('agencydriverslist',$drivers);	               
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }	
        }

        public function tasks(){
        	if($this->_isAgency()){
                $currentagencydetails = $this->getAgencyCategory($this->Auth->user('id'));
                $categoryarr = explode(',', $currentagencydetails->category);
                $task = $this->Scraps->find('all')
                                    ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],'Categorys','Cities'])
                                    ->where([ 'AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.status' => 0],
                                                    ['Scraps.city_id' => $this->Auth->user('city_id')],
                                                    ['Scraps.scrap_category_id IN' => $categoryarr]
                                                ]
                                            ])
                                    ->order(['Scraps.id' => 'DESC']);
                                    // pr($task->toarray()); exit;
                foreach ($task as $key => $value) {
                    $value['address'] = preg_replace('/\s+/S', ' ', $value['address']);
                }
                $this->set('tasks',$task);
                $this->set('jsonnewtasks', json_encode($task));        
                $this->set('jsonagencydetail', json_encode($currentagencydetails));   
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }	
        }

        public function pending(){
        	if($this->_isAgency()){
                $currentagencydetails = $this->getAgencyCategory($this->Auth->user('id'));
                $approvedtask = $this->Scraps->find('all')
                                    ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],'Categorys','Cities'])
                                    ->where([ 'AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.status' => 1],
                                                    ['Scraps.agency_details_id' => $currentagencydetails->id],
                                                ]
                                            ])
                                    ->order(['Scraps.id' => 'DESC']);
                $drivers = $this->Drivers->find('all')
                                        ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname'
                                                ]
                                            ],'Agencys'])
                                        ->where(['AND' => [
                                                        ['Drivers.archived' => 0],
                                                        ['Agencys.users_id' => $this->Auth->user('id')]
                                                    ]
                                                ])
                                        ->order(['Users.fname' => 'ASC']);
                foreach ($approvedtask as $key => $value) {
                    $value['address'] = preg_replace('/\s+/S', ' ', $value['address']);
                }
                $this->set('alldrivers',$drivers);
                $this->set('approvedtasks',$approvedtask);
                $this->set('jsonapprovedtasks', json_encode($approvedtask));        
                $this->set('jsonagencydetail', json_encode($currentagencydetails));                
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }	
        }

        public function assigned(){
            if($this->_isAgency()){
                $currentagencydetails = $this->getAgencyCategory($this->Auth->user('id'));
                $assignedtask = $this->Scraps->find('all')
                                    ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],'Categorys','Drivers.Users','Cities'])
                                    ->where([ 'AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.status' => 2],
                                                    ['Scraps.agency_details_id' => $currentagencydetails->id],
                                                ]
                                            ])
                                    ->order(['Scraps.id' => 'DESC']);
                foreach ($assignedtask as $key => $value) {
                    $value['address'] = preg_replace('/\s+/S', ' ', $value['address']);
                }
                $this->set('assignedtasks',$assignedtask);
                $this->set('jsonassignedtasks', json_encode($assignedtask)); 
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }       
        }

        public function inprocess(){
        	if($this->_isAgency()){
                $currentagencydetails = $this->getAgencyCategory($this->Auth->user('id'));
                $inprocesstask = $this->Scraps->find('all')
                                    ->contain(['Users' => [
                                                'fields' => [
                                                    'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                ]
                                            ],'Categorys','Drivers.Users','Cities'])
                                    ->where([ 'AND' => [
                                                    ['Scraps.archived' => 0],
                                                    ['Scraps.agency_details_id' => $currentagencydetails->id],
                                                    ['OR' => [
                                                                ['Scraps.status' => 3],
                                                                ['Scraps.status' => 4]
                                                            ]                                                            
                                                    ]
                                                ]
                                            ])
                                    ->order(['Scraps.id' => 'DESC']);
                foreach ($inprocesstask as $key => $value) {
                    $value['address'] = preg_replace('/\s+/S', ' ', $value['address']);
                }
                $this->set('inprocesstasks',$inprocesstask);
                $this->set('jsoninprocesstasks', json_encode($inprocesstask));
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }	
        }

        public function viewmap($id = null){
            if(!empty($id)){
                $scrap = $this->Scraps->get($id);                
                $scrapcomplete = $this->Trackings->find()
                                                ->contain([
                                                    'Scraps.Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile'
                                                        ]
                                                    ],
                                                    'Scraps.Drivers.Users' => [
                                                        'fields' => [
                                                            'Users.id','Users.fname','Users.lname','Users.mobile' 
                                                        ]
                                                    ],
                                                    'Scraps.Agencys'                                                    
                                                ])
                                                ->where([
                                                    'Trackings.upload_scraps_id' => $id,
                                                    'Trackings.status' => 5
                                                ])
                                                ->last();   
                $this->set(compact('scrap','scrapcomplete'));       
            }
        }

        public function complete(){
        	if($this->_isAgency()){
                $currentagencydetails = $this->getAgencyCategory($this->Auth->user('id'));
                if($this->request->is('post')){
                    $startdate =  date("Y-m-d", strtotime($this->request->data['startDate']));
                    $enddate =  date("Y-m-d", strtotime($this->request->data['endDate']));
                    $completedtask = $this->Scraps->find()
                                        ->contain(['Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ],'Categorys','Drivers.Users','Cities'])
                                        ->where(['Scraps.archived' => 0, 
                                                    'Scraps.status' => 5,
                                                    'Scraps.agency_details_id' => $currentagencydetails->id,
                                                    'Scraps.modified BETWEEN :start AND :end'
                                                ])
                                        ->order(['Scraps.id' => 'DESC'])
                                        ->bind(':start', new \DateTime($startdate), 'date')
                                        ->bind(':end',   new \DateTime($enddate), 'date'); 
                    $this->set(compact('startdate','enddate'));   
                }else{
                    $completedtask = $this->Scraps->find('all')
                                        ->contain(['Users' => [
                                                    'fields' => [
                                                        'Users.id','Users.fname','Users.lname','Users.mobile','Users.email','Users.address'
                                                    ]
                                                ],'Categorys','Drivers.Users','Cities'])
                                        ->where([ 'AND' => [
                                                        ['Scraps.archived' => 0],
                                                        ['Scraps.status' => 5],
                                                        ['Scraps.agency_details_id' => $currentagencydetails->id],
                                                    ]
                                                ])
                                        ->order(['Scraps.id' => 'DESC']);                    
                }
                foreach ($completedtask as $key => $value) {
                    $value['address'] = preg_replace('/\s+/S', ' ', $value['address']);
                }
                $this->set('completedtasks',$completedtask);
                $this->set('jsoncompletedtasks', json_encode($completedtask));                 
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }	
        }

        public function ajaxacceptscrap(){
            $result['status'] = false;
            if($this->request->is('Ajax')){
                $scrap = $this->Scraps->get($this->request->data['id']);
                if($scrap->status == 0){
                    $scrap->status = 1;
                    $scrap->agency_details_id = $this->request->data['agency_id'];
                    $scrap->modified = new Time();
                    if($this->Scraps->save($scrap)){
                        $result['status'] = true;
                    }else{
                        $result['status'] = false;
                        $result['msg'] = 'Failed to save response, Please Try Again';
                    }
                }else{
                    $result['msg'] = 'Already Accepted';
                }
            }
            $this->jsonResponse($result);
        }

        public function ajaxassignscrap(){
            $result['status'] = false;
            if($this->request->is('Ajax')){
                $scrapassign = $this->Scraps->get($this->request->data['id']);
                if($scrapassign->status == 1 && empty($scrapassign->driver_details_id)){
                    $scrapassign->status = 2;
                    $scrapassign->driver_details_id = $this->request->data['driverid'];
                    $scrapassign->modified = new Time();
                    if($this->Scraps->save($scrapassign)){
                        $result['status'] = true;
                    }else{
                        $result['status'] = false;
                        $result['msg'] = 'Failed to save response, Please Try Again';
                    }    
                }else{
                    $result['msg'] = 'Already Assigned';  
                }
            }
            $this->jsonResponse($result);   
        }
    } 
?>