<?php
    namespace App\Controller;

    use App\Controller\AppController;
    use Cake\Event\Event;
    use Cake\Utility\Security;
    use Cake\Routing\Router;
    use Cake\View\Helper\HtmlHelper;
    use Cake\Validation\Validation;
    use Cake\View\View;
    use View\Helper\FormHelper;
    use Cake\Mailer\MailerAwareTrait;
    use Cake\I18nDate;
    use Cake\Utility\Text;  
    class ScrapsController extends AppController{
        public function initialize(){
            parent::initialize();
            $this->viewBuilder()->layout('AdminManager.dashboardlayout');
            $this->loadComponent('RequestHandler');
            $this->loadModel('Users');
            $this->loadModel('Agencys');
            $this->loadModel('Drivers');
            $this->loadModel('Categorys');
            $this->loadModel('Scraps');
            $this->loadModel('Cities');
            $this->loadModel('Trackings');
        }

        public function beforeFilter(Event $event){
            parent::beforeFilter($event);
        }

        public function index(){
        	if($this->_isAdmin() || $this->_isUser()){
	        	if($this->request->is('post')){
	        		$scrap = $this->Scraps->newEntity();
	        		$this->request->data['users_id'] = $this->Auth->user('id');
	        		$this->request->data['scrap_category_id'] = $this->request->data['category'];
	        		$this->request->data['scrapimg'] = $this->imageupload($this->request->data['uploadedScrap'], 'scrapImage');
	        		$this->Scraps->patchEntity($scrap, $this->request->getData());
	        		if($this->Scraps->save($scrap)){
	        			$this->Flash->success(__('Scrap Details Saved Successfully'));
	                    return $this->redirect(['controller' => 'Users','action' => 'dashboard']);
	        		}
	        		$this->Flash->error(__('Unable to save Scrap Try Again.'));		
	        	}
            }else{
                $this->Flash->error(__('Access Denied.!!!'));
                return $this->redirect($this->Auth->logout()); 
            }
        }
    } 
?>